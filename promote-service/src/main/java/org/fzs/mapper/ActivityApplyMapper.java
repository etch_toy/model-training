package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.fzs.common.entity.ActivityApply;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linjintao
 * @since 2021-04-07
 */
@Mapper
public interface ActivityApplyMapper extends BaseMapper<ActivityApply> {

}
