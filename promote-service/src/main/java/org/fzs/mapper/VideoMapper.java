package org.fzs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.fzs.common.entity.Video;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linjintao
 * @since 2021-03-25
 */
@Mapper
public interface VideoMapper extends BaseMapper<Video> {

}
