package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.fzs.common.entity.EnterpriseActivity;
import org.fzs.common.vo.EnterpriseActivityVo;

import java.time.LocalDateTime;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linjintao
 * @since 2021-03-14
 */
@Mapper
public interface EnterpriseActivityMapper extends BaseMapper<EnterpriseActivity> {

    @Select("<script>select e.*, (not isnull(a.user_id)) as is_join from enterprise_activity e " +
            "left join activity_apply a ON e.id = a.enterprise_activity_id and a.user_id = #{vo.userId} " +
            "where e.deleted = 0 " +
            "<if test='now != null'> and #{now} between start_time and end_time </if>" +
            "<if test='vo.enterpriseName != null'> " +
            "and e.enterprise_name like concat('%', #{vo.enterpriseName},'%') </if> " +
            "<if test='vo.name != null'>" +
            "and e.name like concat('%', #{vo.name},'%') </if> " +
            "order by e.start_time desc  </script>")
    IPage<EnterpriseActivityVo> queryByUserId(@Param("vo") EnterpriseActivityVo vo,
                                              @Param("page") Page<EnterpriseActivity> page,
                                              @Param("now") LocalDateTime now);

    @Update("update enterprise_activity set join_count = join_count + #{num} where id = #{id} ")
    void plusJoinCount(@Param("id") Integer id, @Param("num") Integer num);
}
