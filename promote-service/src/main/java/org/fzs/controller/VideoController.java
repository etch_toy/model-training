package org.fzs.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fzs.common.entity.Video;
import org.fzs.common.service.IVideoService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.Constants;
import org.fzs.common.utils.ResultCode;
import org.fzs.util.Progress;
import org.fzs.util.UploadVideoDemo;
import org.fzs.util.VideoPlay;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-25
 */
@RestController
@RequestMapping("/promote/video")
@RequiredArgsConstructor
public class VideoController {
    private static final String VIDEO_SUFFIX = "AVI|avi|FLV|flv|MP4|mp4|MKV|mkv|MOV|mov";
    @Value("${oss.accessKeyId}")
    private  String accessKeyId;
    @Value("${oss.accessKeySecret}")
    private String accessKeySecret;
    private final IVideoService videoService;
    private final RedisTemplate<String, String> redisTemplate;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final UploadVideoDemo uploadVideoDemo;
    private final VideoPlay videoPlay;

    @ApiOperation("请求生成随机码, 用于获取视频上传进度")
    @GetMapping("/getRandomKey")
    @ApiImplicitParams({
    })
    public CommonResult getRandomCode() throws IOException {
        String preOriginCode = UUID.randomUUID().toString().replace("-", "").substring(0, 8);
        String originCode = preOriginCode + LocalDateTime.now().plusMinutes(5).toEpochSecond(ZoneOffset.of("+8"));
        String imageCode = originCode + "-" + passwordEncoder.encode(originCode);
        return CommonResult.success(imageCode);
    }

    @ApiOperation("获取视频上传进度")
    @GetMapping("/getUploadProgress")
    public CommonResult getUploadProgress(@RequestParam String randomKey) {
        String object = redisTemplate.opsForValue().get(Constants.VIDEO_UPLOAD_KEY_PRE + randomKey);
        if(object == null) return CommonResult.badArgument("不存在的上传文件");
        Progress progress = JSONObject.parseObject(object, Progress.class);
        if(progress.getErrorMsg() != null) return CommonResult.failed(ResultCode.FAILED, "上传文件发生错误,请重新上传");
        return CommonResult.success(progress);
    }

    @ApiOperation("获取视频链接")
    @GetMapping("/getVideoUrl/{videoId}")
    public CommonResult getVideoUrl(@PathVariable String videoId){
        return CommonResult.success(videoPlay.getPlayUrl(videoId));
    }

    @ApiOperation("上传视频文件")
    @PostMapping("/uploadVideo")
    public CommonResult uploadVideo(@RequestParam MultipartFile file, @RequestParam(required = false) String randomKey){
        if (file == null || file.isEmpty()) {
            return CommonResult.badArgument("文件不能为空");
        }
        if (file.getOriginalFilename() == null) {
            return CommonResult.badArgument("原文件名为空！");
        }
        if (file.getSize() > 1024 * 1024 * 100) {
            return CommonResult.badArgument("文件大小不能大于100m");
        }
        if(randomKey != null && !verifyRandomKey(randomKey)){
            return CommonResult.badArgument("随机码无效或已过期");
        }

        String suffix = file.getOriginalFilename()
                .substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        if (!Pattern.matches(VIDEO_SUFFIX, suffix)) {
            return CommonResult.badArgument("不支持的文件格式！");
        }
        String fileName = UUID.randomUUID().toString().replace("-", "") + "." + suffix;

        String videoId;
        try {
            videoId = uploadVideoDemo.testUploadStream(accessKeyId, accessKeySecret,
                    fileName, file.getOriginalFilename(), file.getInputStream(), file.getSize(), randomKey);
        } catch (IOException e) {
            return CommonResult.failed(ResultCode.FAILED, "上传文件发生错误");
        }
        return CommonResult.success(videoId);
    }

    @ApiOperation("分页查询视频")
    @GetMapping("/page")
    public CommonResult page(@RequestParam(required = false) Integer courseId,
                             @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                             @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<Video> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        QueryWrapper<Video> wrapper = new QueryWrapper<>();
        if(courseId != null)
            wrapper.eq("course_id", courseId);
        return CommonResult.success(videoService.page(page, wrapper));
    }

    @ApiOperation("分页查询视频")
    @GetMapping("/{id}")
    public CommonResult detail(@PathVariable Integer id){
        return CommonResult.success(videoService.getById(id));
    }

    @ApiOperation("新增或修改视频")
    @PostMapping()
    public CommonResult saveOrUpdate(@Validated @RequestBody Video video, BindingResult bindingResult){
        videoService.saveOrUpdate(video);
        return CommonResult.success();
    }

    @ApiOperation("删除视频")
    @DeleteMapping("/{id}")
    public CommonResult remove(@PathVariable Integer id){
        videoService.removeById(id);
        return CommonResult.success();
    }






    private boolean verifyRandomKey(String randomCode){
        String[] split = randomCode.split("-");
        if(split.length != 2) {
            return false;
        }
        String originCode = split[0];
        String signature = split[1];
        if(!passwordEncoder.matches(originCode, signature)) {
            return false;
        }
        String time = originCode.substring(8);
        Long timeStamp = Long.valueOf(time);
        LocalDateTime end = LocalDateTime.ofEpochSecond(timeStamp, 0, ZoneOffset.of("+8"));
        if(LocalDateTime.now().isAfter(end)) {
            return false;
        }
        return true;
    }
}
