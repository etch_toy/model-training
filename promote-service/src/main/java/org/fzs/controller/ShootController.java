package org.fzs.controller;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.fzs.common.entity.Shoot;
import org.fzs.common.service.IShootService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.QRCodeUtils;
import org.fzs.util.Url;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-13
 */
@RestController
@RequestMapping("/promote")
public class ShootController {
    @Autowired
    private IShootService shootService;


    @Value("${oss.endpoint}")
    private String endpoint;
    @Value("${oss.accessKeyId}")
    private  String accessKeyId;
    @Value("${oss.accessKeySecret}")
    private String accessKeySecret;
    @Value("${oss.bucketName}")
    private String bucketName;
    @Value("${file.logo}")
    private String logoPath;
    @Value("${file.destPath}")
    private String destPath;

    private static final String IMAGE_SUFFIX = "jpg|JPG|png|PNG|jpeg|JPEG|bmp|BMP|tiff|TIFF|svg|SVG";


    @ApiOperation("分页查询拍摄照片")
    @GetMapping("/shoot/page")
    public CommonResult page(@RequestParam(required = false) Integer courseId,
                                      @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                      @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<Shoot> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        QueryWrapper<Shoot> wrapper = new QueryWrapper<>();
        if(courseId != null)
            wrapper.eq("course_id", courseId);
        return CommonResult.success(shootService.page(page, wrapper));
    }

    @ApiOperation("按id查询拍摄照片")
    @GetMapping("/shoot/{id}")
    public CommonResult detail(@PathVariable Integer id){
        return CommonResult.success(shootService.getById(id));
    }

    @ApiOperation("新增或修改拍摄图片")
    @PostMapping("/shoot")
    public CommonResult saveOrUpdate(@Validated @RequestBody Shoot shoot, BindingResult bindingResult){
        shootService.saveOrUpdate(shoot);
        return CommonResult.success();
    }

    @ApiOperation("新增或修改拍摄图片")
    @PostMapping("/shoot/{courseId}")
    public CommonResult saveOrUpdateByCourse(@PathVariable Integer courseId,
                                             @Validated @RequestBody List<Shoot> shoots,
                                             BindingResult bindingResult){

        if(shoots == null) return CommonResult.success();
        QueryWrapper<Shoot> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        shootService.remove(wrapper);
        shoots.forEach(item->item.setCourseId(courseId));
        shootService.saveOrUpdateBatch(shoots);
        return CommonResult.success();
    }

    @ApiOperation("删除图片")
    @DeleteMapping("/shoot/{id}")
    public CommonResult remove(@PathVariable Integer id){
        shootService.removeById(id);
        return CommonResult.success();
    }

    @ApiOperation("上传图片文件")
    @PostMapping("uploadImage")
    public CommonResult<String> uploadImage(@RequestParam MultipartFile file){
        if (file == null || file.isEmpty()) {
            return CommonResult.badArgument("文件不能为空");
        }
        if (file.getOriginalFilename() == null) {
            return CommonResult.badArgument("原文件名为空！");
        }
        if (file.getSize() > 1024 * 1024 * 10) {
            return CommonResult.badArgument("文件大小不能大于10m");
        }

        String suffix = file.getOriginalFilename()
                .substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        if (!Pattern.matches(IMAGE_SUFFIX, suffix)) {
            return CommonResult.badArgument("不支持的文件格式！");
        }
        String fileName = UUID.randomUUID().toString().replace("-", "") + "." + suffix;
        try {
            upload(bucketName, fileName, suffix, file.getInputStream());
        } catch (IOException e) {
            return CommonResult.failed("上传失败，请重试");
        }
        return CommonResult.success(bucketName + "." + endpoint + "/" +fileName);
    }

    @ApiOperation("生成二维码")
    @PostMapping("/getQRCode")
    public CommonResult getQRCode(@RequestBody Url url, @ApiIgnore HttpServletResponse response) throws Exception {

        if(url.getTarget() == null)
            return CommonResult.badArgument("参数不能为空");
        String fileName = QRCodeUtils.encode(url.getTarget(), logoPath, destPath, true);
        File file = new File(destPath + "/" + fileName);
        FileInputStream fileInputStream = new FileInputStream(file);
        upload(bucketName, fileName, "jpg", fileInputStream);
        fileInputStream.close();
        return CommonResult.success(bucketName + "." + endpoint + "/" +fileName);
    }

    private void upload(String bucketName, String fileName, String suffix, InputStream is){
        ObjectMetadata objectMetadata= new ObjectMetadata();
        objectMetadata.setContentType("image/" + suffix);
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(new PutObjectRequest(bucketName, fileName, is));
        // 关闭OSSClient。
        ossClient.shutdown();
    }


}
