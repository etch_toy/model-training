package org.fzs.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.fzs.common.entity.ActivityApply;
import org.fzs.common.entity.EnterpriseActivity;
import org.fzs.common.entity.User;
import org.fzs.common.service.IActivityApplyService;
import org.fzs.common.service.IEnterpriseActivityService;
import org.fzs.common.service.IUserService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.ExcelUtils;
import org.fzs.common.utils.UserUtils;
import org.fzs.common.vo.ApplyUserVo;
import org.fzs.common.vo.EnterpriseActivityVo;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-14
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/promote/enterprise")
public class EnterpriseActivityController {

    private final IEnterpriseActivityService enterpriseActivityService;
    private final IActivityApplyService activityApplyService;
    @Reference
    private IUserService userService;

    @ApiOperation("分页查询企业活动")
    @GetMapping("page")
    public CommonResult page(@RequestParam(required = false) String enterpriseName,
                             @RequestParam(required = false) String name,
                             @RequestParam(required = false) Boolean isValid,
                            @ApiIgnore Authentication authentication,
                             @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                             @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Integer userId;
        User user = UserUtils.getLoginUser(authentication);
        userId = user == null ? null : user.getId();
        Page<EnterpriseActivity> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        EnterpriseActivityVo vo = new EnterpriseActivityVo();
        vo.setUserId(userId);
        if(!StringUtils.isBlank(name)) vo.setName(name);
        if(!StringUtils.isBlank(enterpriseName)) vo.setEnterpriseName(enterpriseName);
        return CommonResult.success(enterpriseActivityService.getPage(vo, page));
    }

    @ApiOperation("管理员分页查询企业活动")
    @GetMapping("pageByAdmin")
    public CommonResult pageByAdmin(@RequestParam(required = false) String enterpriseName,
                             @RequestParam(required = false) String name,
                             @ApiIgnore Authentication authentication,
                             @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                             @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Integer userId;
        User user = UserUtils.getLoginUser(authentication);
        userId = user == null ? null : user.getId();
        Page<EnterpriseActivity> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        EnterpriseActivityVo vo = new EnterpriseActivityVo();
        vo.setUserId(userId);
        if(!StringUtils.isBlank(name)) vo.setName(name);
        if(!StringUtils.isBlank(enterpriseName)) vo.setEnterpriseName(enterpriseName);
        return CommonResult.success(enterpriseActivityService.getPageByAdmin(vo, page));
    }

    @ApiOperation("导出所有企业活动")
    @GetMapping("/exportAll")
    public void exportExcel(HttpServletResponse response) throws IOException {
        List<EnterpriseActivity> students = enterpriseActivityService.list();
        ExcelUtils.exportExcel(students, "企业活动", "企业活动", EnterpriseActivity.class,"企业活动", response);
    }

    @ApiOperation("查询参加该企业活动的所有用户")
    @GetMapping("/AllUser/{id}")
    public CommonResult<List<ApplyUserVo>> AllUser(@PathVariable Integer id,  HttpServletResponse response) throws IOException {
        List<ActivityApply> list = activityApplyService.getByActivityId(id);
        List<Integer> ids = list.stream().map(ActivityApply::getUserId).collect(Collectors.toList());
        List<User> users = userService.listByIds(ids);
        HashMap<Integer, User> idToUser = new HashMap<>();
        users.forEach(item -> idToUser.put(item.getId(), item));
        List<ApplyUserVo> voList = list.stream().map(item -> {
            Integer userId = item.getUserId();
            User user = idToUser.get(userId);
            ApplyUserVo vo = new ApplyUserVo();
            BeanUtils.copyProperties(user, vo);
            vo.setRemark(item.getRemark());
            return vo;
        }).collect(Collectors.toList());
        return CommonResult.success(voList);
    }

    @ApiOperation("导出企业活动的所有用户")
    @GetMapping("/exportAllUser/{id}")
    public void exportExcelAllUser(@PathVariable Integer id,  HttpServletResponse response) throws IOException {
        EnterpriseActivity enterpriseActivity = enterpriseActivityService.getById(id);
        if(enterpriseActivity == null) return;
        List<ActivityApply> list = activityApplyService.getByActivityId(id);
        List<Integer> ids = list.stream().map(ActivityApply::getUserId).collect(Collectors.toList());
        List<User> users = userService.listByIds(ids);
        HashMap<Integer, User> idToUser = new HashMap<>();
        users.forEach(item -> idToUser.put(item.getId(), item));
        List<ApplyUserVo> voList = list.stream().map(item -> {
            Integer userId = item.getUserId();
            User user = idToUser.get(userId);
            ApplyUserVo vo = new ApplyUserVo();
            BeanUtils.copyProperties(user, vo);
            vo.setRemark(item.getRemark());
            return vo;
        }).collect(Collectors.toList());
        String title = enterpriseActivity.getName() + "参加人员";
        ExcelUtils.exportExcel(voList, title + "名单", title+ "名单",
                ApplyUserVo.class,title+ "名单", response);
    }

    @ApiOperation("企业活动详情")
    @GetMapping("{id}")
    public CommonResult detail(@PathVariable Integer id){
        return CommonResult.success(enterpriseActivityService.getById(id));
    }

    @ApiOperation("新增或修改企业活动")
    @PostMapping()
    public CommonResult saveOrUpdate(@Validated @RequestBody EnterpriseActivity enterpriseActivity,
                                     BindingResult bindingResult){
        enterpriseActivityService.saveOrUpdate(enterpriseActivity);
        return CommonResult.success();
    }

    @ApiOperation("删除企业活动")
    @DeleteMapping("{id}")
    public CommonResult remove(@PathVariable Integer id){
        enterpriseActivityService.removeById(id);
        return CommonResult.success();
    }

    @ApiOperation("企业活动备注")
    @PostMapping("remark")
    public CommonResult setRemark(@RequestBody ActivityApply activityApply, @ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        QueryWrapper<ActivityApply> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", user.getId())
                .eq("enterprise_activity_id", activityApply.getEnterpriseActivityId());
        List<ActivityApply> list = activityApplyService.list(wrapper);
        if(list.isEmpty()) return CommonResult.badArgument("您未参加此活动");
        activityApply.setUserId(user.getId());
        UpdateWrapper<ActivityApply> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("user_id", user.getId())
                .eq("enterprise_activity_id", activityApply.getEnterpriseActivityId())
                .set("remark", activityApply.getRemark());
        activityApplyService.update(updateWrapper);
        return CommonResult.success();
    }

    @ApiOperation("报名参加企业活动")
    @GetMapping("apply/{id}")
    public CommonResult apply(@PathVariable Integer id, @ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        EnterpriseActivity activity = enterpriseActivityService.getById(id);
        if(activity == null) return CommonResult.badArgument("没有这个活动");
        if(activity.getStartTime() != null && activity.getStartTime().isAfter(LocalDateTime.now()))
            return CommonResult.badArgument("活动还未开始");
        if(activity.getEndTime() != null && activity.getEndTime().isBefore(LocalDateTime.now()))
            return CommonResult.badArgument("活动已经结束");

        QueryWrapper<ActivityApply> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", user.getId())
                .eq("enterprise_activity_id", id);
        if(!activityApplyService.list(wrapper).isEmpty()) return CommonResult.success();
        ActivityApply apply = ActivityApply.builder().enterpriseActivityId(id).userId(user.getId()).build();
        boolean save = activityApplyService.save(apply);
        if(save) enterpriseActivityService.incrJoinCount(id);
        return CommonResult.success();
    }

    @ApiOperation("取消报名")
    @GetMapping("cancelApply/{id}")
    public CommonResult cacheApply(@PathVariable Integer id, @ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        QueryWrapper<ActivityApply> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", user.getId())
                .eq("enterprise_activity_id", id);
        boolean remove = activityApplyService.remove(wrapper);
        if(remove) enterpriseActivityService.descJoinCount(id);
        return CommonResult.success();
    }


    @ApiOperation("我参加的企业活动")
    @GetMapping("myApply")
    public CommonResult myApply(@ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        QueryWrapper<ActivityApply> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", user.getId());
        List<ActivityApply> list = activityApplyService.list(wrapper);
        List<Integer> ids = list.stream().map(ActivityApply::getEnterpriseActivityId).collect(Collectors.toList());
        if(ids.isEmpty()) return CommonResult.success(new ArrayList<>());
        List<EnterpriseActivity> enterpriseActivities = enterpriseActivityService.listByIds(ids);
        List<EnterpriseActivityVo> voList = enterpriseActivities.stream().map(enterpriseActivity -> {
            EnterpriseActivityVo vo = new EnterpriseActivityVo();
            BeanUtils.copyProperties(enterpriseActivity, vo);
            for (ActivityApply apply : list) {
                if (apply.getEnterpriseActivityId().equals(vo.getId())) {
                    vo.setRemark(apply.getRemark());
                    break;
                }
            }
            return vo;
        }).collect(Collectors.toList());
        return CommonResult.success(voList);
    }

}
