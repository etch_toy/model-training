package org.fzs.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fzs.common.entity.EnterpriseActivity;
import org.fzs.common.entity.HomePage;
import org.fzs.common.service.IHomePageService;
import org.fzs.common.utils.CommonResult;
import org.fzs.service.HomePageServiceImpl;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/promote/homepage")
public class HomePageController {

    private final IHomePageService homePageService;
    private final Integer STUDENT_HOMEPAGE_ID = 1;
    private final Integer TEACHER_HOMEPAGE_ID = 2;

    @ApiOperation("学生首页")
    @GetMapping("studentHomepage")
    public CommonResult studentHome(){
        return CommonResult.success(homePageService.getById(STUDENT_HOMEPAGE_ID));
    }

    @ApiOperation("教师首页")
    @GetMapping("teacherHomepage")
    public CommonResult teacherHome(){
        return CommonResult.success(homePageService.getById(TEACHER_HOMEPAGE_ID));
    }

//    @ApiOperation("分页查询拍摄照片")
//    @GetMapping("{id}")
//    public CommonResult detail(@PathVariable Integer id){
//        return CommonResult.success(homePageService.getById(id));
//    }

    @ApiOperation("修改学生首页")
    @PostMapping("studentHomepage")
    public CommonResult saveOrUpdateStudent(@Validated @RequestBody HomePage homePage,
                                     BindingResult bindingResult){
        homePage.setId(STUDENT_HOMEPAGE_ID);
        homePageService.saveOrUpdate(homePage);
        return CommonResult.success();
    }

    @ApiOperation("修改教师首页")
    @PostMapping("teacherHomepage")
    public CommonResult saveOrUpdateTeacher(@Validated @RequestBody HomePage homePage,
                                     BindingResult bindingResult){
        homePage.setId(TEACHER_HOMEPAGE_ID);
        homePageService.saveOrUpdate(homePage);
        return CommonResult.success();
    }

}
