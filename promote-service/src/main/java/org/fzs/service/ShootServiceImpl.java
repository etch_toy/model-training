package org.fzs.service;

import org.fzs.common.entity.Shoot;
import org.fzs.common.service.IShootService;
import org.fzs.mapper.ShootMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-13
 */
@Service
public class ShootServiceImpl extends ServiceImpl<ShootMapper, Shoot> implements IShootService {

}
