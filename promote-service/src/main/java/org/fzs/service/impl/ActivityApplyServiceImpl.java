package org.fzs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.fzs.common.entity.ActivityApply;
import org.fzs.common.service.IActivityApplyService;
import org.fzs.mapper.ActivityApplyMapper;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-04-07
 */
@Service
public class ActivityApplyServiceImpl extends ServiceImpl<ActivityApplyMapper, ActivityApply> implements IActivityApplyService {

    @Override
    public List<ActivityApply> getByActivityId(Integer activityId) {
        QueryWrapper<ActivityApply> wrapper = new QueryWrapper<>();
        wrapper.eq("enterprise_activity_id", activityId);
        return this.list(wrapper);
    }
}
