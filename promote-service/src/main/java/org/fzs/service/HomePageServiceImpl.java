package org.fzs.service;

import org.fzs.common.entity.HomePage;
import org.fzs.common.service.IHomePageService;
import org.fzs.mapper.HomePageMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-16
 */
@Service
public class HomePageServiceImpl extends ServiceImpl<HomePageMapper, HomePage> implements IHomePageService {

}
