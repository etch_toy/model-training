package org.fzs.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.Service;
import org.fzs.common.entity.EnterpriseActivity;
import org.fzs.common.service.IEnterpriseActivityService;
import org.fzs.common.vo.EnterpriseActivityVo;
import org.fzs.mapper.EnterpriseActivityMapper;

import java.time.LocalDateTime;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-14
 */
@Service
@RequiredArgsConstructor
public class EnterpriseActivityServiceImpl extends ServiceImpl<EnterpriseActivityMapper, EnterpriseActivity>
        implements IEnterpriseActivityService {
    private final EnterpriseActivityMapper enterpriseActivityMapper;


    @Override
    public IPage<EnterpriseActivityVo> getPage(EnterpriseActivityVo vo, Page page) {
        return enterpriseActivityMapper.queryByUserId(vo, page, LocalDateTime.now());
    }

    @Override
    public IPage<EnterpriseActivityVo> getPageByAdmin(EnterpriseActivityVo vo, Page page) {
        return enterpriseActivityMapper.queryByUserId(vo, page, null);
    }

    @Override
    public void descJoinCount(Integer id) {
        enterpriseActivityMapper.plusJoinCount(id, -1);
    }

    @Override
    public void incrJoinCount(Integer id) {
        enterpriseActivityMapper.plusJoinCount(id, 1);
    }
}
