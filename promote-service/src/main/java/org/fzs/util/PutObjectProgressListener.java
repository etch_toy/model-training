package org.fzs.util;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.event.ProgressEvent;
import com.aliyun.oss.event.ProgressEventType;
import com.aliyun.vod.upload.impl.VoDProgressListener;
import com.fasterxml.jackson.core.JsonEncoding;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.fzs.common.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 上传进度回调方法类
 * 当您开启上传进度回调时该事件回调才会生效。
 * OSS分片上传成功或失败均触发相应的回调事件，您可根据业务逻辑处理相应的事件回调。
 * 当创建音视频信息成功后，此上传进度回调中的videoId为本次上传生成的视频ID，您可以根据视频ID进行音视频管理。
 * 当创建图片信息成功后，此上传进度回调中的ImageId为本次上传生成的图片ID，您可以根据视频ID进行图片管理。
 */
@Slf4j
@Scope(scopeName = "prototype")
@Component
@RequiredArgsConstructor
public class PutObjectProgressListener implements VoDProgressListener {
    /**
     * 已成功上传至OSS的字节数
     */
    private long bytesWritten = 0;
    /**
     * 原始文件的总字节数
     */
    private long totalBytes = -1;
    /**
     * 本次上传成功标记
     */
    private boolean succeed = false;
    /**
     * 视频ID
     */
    private String videoId;
    /**
     * 图片ID
     */
    private String imageId;

    /**
     * redis的key
     */
    private String uploadKey;
    private Progress progress = new Progress();

    private final RedisTemplate<String, String> redisTemplate;
    private final VideoPlay videoPlay;

//    @PostConstruct
//    void init(){
//        uploadKey = UUID.randomUUID().toString().replace("-", "").substring(8);
//    }
    private void updateProgressToRedis(){
        redisTemplate.opsForValue().set(Constants.VIDEO_UPLOAD_KEY_PRE + uploadKey,
                JSONObject.toJSONString(progress), 10, TimeUnit.MINUTES);

    }

    public void putRedisErrorMessage(String errorMsg){
        progress.setErrorMsg(errorMsg);
        updateProgressToRedis();

    }
    public void progressChanged(ProgressEvent progressEvent) {
        long bytes = progressEvent.getBytes();
        ProgressEventType eventType = progressEvent.getEventType();
        switch (eventType) {
            // 开始上传事件
            case TRANSFER_STARTED_EVENT:
                if (videoId != null) {
                    log.debug("Start to upload videoId " + videoId + "......");
                    progress.setVideoId(videoId);
                }
                if (imageId != null) {
                    log.debug("Start to upload imageId " + imageId + "......");
                }

                break;
            // 计算待上传文件总大小事件通知，只有调用本地文件方式上传时支持该事件
            case REQUEST_CONTENT_LENGTH_EVENT:
                this.totalBytes = bytes;
                log.debug(this.totalBytes + "bytes in total will be uploaded to OSS.");
                break;
            // 已经上传成功文件大小事件通知
            case REQUEST_BYTE_TRANSFER_EVENT:
                this.bytesWritten += bytes;
                if (this.totalBytes != -1) {
                    int percent = (int) (this.bytesWritten * 100.0 / this.totalBytes);
                    log.debug(bytes + " bytes have been written at this time, upload progress: " +
                            percent + "%(" + this.bytesWritten + "/" + this.totalBytes + ")");
                    progress.setRadio(percent);
                    // 已经上传进度的存入redis
                    updateProgressToRedis();
                } else {
                    log.debug(bytes + " bytes have been written at this time, upload sub total : " +
                            "(" + this.bytesWritten + ")");
                }
                break;
            // 文件全部上传成功事件通知
            case TRANSFER_COMPLETED_EVENT:
                this.succeed = true;
                if (videoId != null) {
                    String playUrl = videoPlay.getPlayUrl(videoId);
                    progress.setUrl(playUrl);
                    updateProgressToRedis();
                    log.debug("Succeed to upload videoId " + videoId + " , " + this.bytesWritten + " bytes have been transferred in total.");
                }
                if (imageId != null) {
                    log.debug("Succeed to upload imageId " + imageId + " , " + this.bytesWritten + " bytes have been transferred in total.");
                }
                break;
            // 文件上传失败事件通知
            case TRANSFER_FAILED_EVENT:
                if (videoId != null) {
                    log.debug("Failed to upload videoId " + videoId + " , " + this.bytesWritten + " bytes have been transferred.");
                }
                if (imageId != null) {
                    log.debug("Failed to upload imageId " + imageId + " , " + this.bytesWritten + " bytes have been transferred.");
                }

                break;

            default:
                break;
        }
    }

    public boolean isSucceed() {
        return succeed;
    }

    public void onVidReady(String videoId) {
        setVideoId(videoId);
    }

    public void onImageIdReady(String imageId) {
        setImageId(imageId);
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getUploadKey() {
        return uploadKey;
    }

    public void setUploadKey(String uploadKey) {
        this.uploadKey = uploadKey;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }
}

