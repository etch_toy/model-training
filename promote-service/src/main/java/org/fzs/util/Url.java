package org.fzs.util;

import lombok.Data;

@Data
public class Url {
    private String target;
}
