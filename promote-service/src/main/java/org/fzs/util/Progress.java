package org.fzs.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Progress {
    @ApiModelProperty(value = "完成百分率，整数")
    Integer radio;
    @ApiModelProperty(value = "视频id")
    String videoId;
    @ApiModelProperty(value = "错误信息")
    String errorMsg;
    @ApiModelProperty(value = "视频url", notes = "视频上传完成后才会有")
    String url;
}
