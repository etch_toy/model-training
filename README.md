# 少儿模特培训机构管理与推广平台





## 项目架构

![image-20210520120044451](architecture.png)



## 各模块功能

common：公有模块，存放所有服务公用的实体类，service接口以及工具类等。

uaa-service：授权中心，用于用户的认证授权。登录等.在user包下则是原本user-service的功能，包括用户信息的服务，也包括用户的注册

gateway：网关。目前的鉴权是在网关进行，各服务默认拒绝没有认证过的请求。

course-service：课程服务跟通知服务。

promote-service: 推广相关的服务，包括图片，视频的上传以及企业活动等的添加

带service的服务需要单独部署微服务。


## 需要启动的

nacos服务中心：本地需要搭建nacos在8848端口，作为各个服务的注册中心

redis：大部分服务都需要redis。

mysql：

## 登录

登录是使用spring-security-oauth做的。

首先获取随机码（因为本系统不使用session机制），然后把随机码作为参数去获取验证码

http://localhost:8004/uaa/verify/getImageCode


然后通过POST请求去授权中心，获取令牌

http://localhost:8004/uaa/oauth/token?client_id=fzs506&client_secret=fzs506_secret&username=aaa&password=aaa&grant_type=password&verify=验证码

注意把上面的验证码改成对应的验证码，之后填写上随机码，用户名跟密码的话可以自由注册，client_id跟client_secret目前只用上面的两个。

之后将访问令牌放在请求头进行访问就行了。



## 获取当前登录对象

在controller中传入Authentication，并通过UserUtils获取当前登录的用户信息（注意，信息不包括密码），你可以通过id去数据库查询。若返回null，则代表用户不存在。





## 微服务调用

微服务使用的是spring-alibaba的一套，因此服务调用使用的是dubbo，并以nacos作为注册中心。





## controller参数校验

参数校验使用的是hibernate.validator。注意，如果需要参数校验，请把最后一个参数命名为bindingResult。并把要校验的对象加上@Validated注解，如下面所示

```java
    @PostMapping("saveOrUpdate")
    public CommonResult<Integer> saveOrUpdate(@Validated @RequestBody UserDTO user, BindingResult bindingResult){
        return CommonResult.success(userService.addUser(user));
    }
```

实体类中

```java
public class User implements Serializable {
	@ApiModelProperty(value = "密码")
    @Length(max=20, min = 6, message = "密码长度必须在6-20个字符之间")
    private String password;
}
```

**最后，本系统是毕设作品，由于时间不够以及开发双方都是后端人员，前端水平较差，导致前后台接口等设计的不合理（无法使用session之类的）等问题，本系统的很多代码，接口设计都并不合理，很多自己写的跟屎一样，开发完的我都没脸看了，看看就行***

