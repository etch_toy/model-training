package org.fzs.common.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.fzs.common.entity.EnterpriseActivity;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseActivityVo extends EnterpriseActivity {
    @ApiModelProperty(value = "是否已加入")
    Boolean isJoin;
    @ApiModelProperty(value = "备注信息")
    String remark;

    @JsonIgnore
    @ApiModelProperty(value = "用户id，不在前端使用", hidden = true)
    transient Integer userId;
}
