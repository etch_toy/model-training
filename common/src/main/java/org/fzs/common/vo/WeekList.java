package org.fzs.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WeekList {
    List<WeekCourseTimetable> monday;
    List<WeekCourseTimetable> tuesday;
    List<WeekCourseTimetable> wednesday;
    List<WeekCourseTimetable> thursday;
    List<WeekCourseTimetable> friday;
    List<WeekCourseTimetable> saturday;
    List<WeekCourseTimetable> sunday;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "查询结果的开始日期")
    LocalDate startDate;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "查询结果的结束日期")
    LocalDate endDate;

    public static WeekList emptyData(){
        return WeekList.builder()
                .monday(new ArrayList<>())
                .thursday(new ArrayList<>())
                .wednesday(new ArrayList<>())
                .tuesday(new ArrayList<>())
                .friday(new ArrayList<>())
                .saturday(new ArrayList<>())
                .sunday(new ArrayList<>())
                .build();
    }
}
