package org.fzs.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.fzs.common.entity.Course;
import org.fzs.common.entity.CourseContent;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="课程详情", description="")
public class CourseVo extends Course {
    @ApiModelProperty(value = "课程章节列表")
    List<CourseContent> contentList;
    @ApiModelProperty(value = "章节数")
    Integer contentCount;
}
