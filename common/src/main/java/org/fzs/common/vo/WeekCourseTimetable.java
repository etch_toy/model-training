package org.fzs.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WeekCourseTimetable {
    @ApiModelProperty(value = "课程名")
    String courseName;
    @ApiModelProperty(value = "课程id")
    Integer courseId;
    @ApiModelProperty(value = "课程表id")
    Integer courseTimetableId;
    @ApiModelProperty(value = "上课时间")
    @JsonFormat(pattern = "HH:mm:ss", timezone = "GMT+8")
    private LocalTime classTime;

    @ApiModelProperty(value = "结课时间", hidden = true)
    @JsonIgnore
    private LocalDateTime endTime;

    @ApiModelProperty(value = "开始时间", hidden = true)
    @JsonIgnore
    private LocalDateTime startTime;

}
