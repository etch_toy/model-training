package org.fzs.common.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.fzs.common.entity.User;

@Data
public class ApplyUserVo extends User {
    @ApiModelProperty(value = "备注信息")
    @Excel(name = "备注", orderNum = "8", width = 100)
    String remark;
}
