package org.fzs.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.fzs.common.entity.User;

@Data
@Accessors(chain = true)
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class AuditVo extends User {

    @ApiModelProperty(value = "是否已通过")
    Boolean  isPass;
}
