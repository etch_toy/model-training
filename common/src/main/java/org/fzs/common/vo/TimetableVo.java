package org.fzs.common.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TimetableVo {
    static final LocalTime FIRST = LocalTime.of(8, 0);
    static final LocalTime SECOND = LocalTime.of(10, 0);
    static final LocalTime THREE = LocalTime.of(14, 0);
    static final LocalTime FOUR = LocalTime.of(16, 0);
    static final LocalTime FIVE = LocalTime.of(20, 0);
    String classTime;
    String monday;
    String tuesday;
    String wednesday;
    String thursday;
    String friday;
    String saturday;
    String sunday;
}
