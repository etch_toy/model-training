package org.fzs.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.fzs.common.entity.CourseTimetable;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="课程对象")
public class CourseTimetableVo extends CourseTimetable {

    @ApiModelProperty(value = " 代表周几需要上课", example = "1,3,5,7")
    private List<Integer> weekDayList;

    @ApiModelProperty(value = "结课日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate endDate;

    @ApiModelProperty(value = "开课日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @NotNull(message = "开课时间不能为空")
    private LocalDate startDate;

    private String[] dateArray = new String[2];

    @Override
    public CourseTimetable setEndTime(LocalDateTime endTime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        CourseTimetable temp = super.setEndTime(endTime);
        if(endTime != null) {
            endDate = endTime.toLocalDate();
            dateArray[1] =  df.format(endDate);
        }
//        endTime.format(new (""))
        return temp;
    }


    @Override
    public CourseTimetable setStartTime(LocalDateTime startTime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        CourseTimetable temp = super.setStartTime(startTime);
        if(startTime != null){
            startDate = startTime.toLocalDate();
            dateArray[0] = df.format(startDate);
        }
        return temp;
    }
}
