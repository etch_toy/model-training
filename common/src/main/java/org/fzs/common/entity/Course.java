package org.fzs.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Course对象", description="")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课程名称")
    @NotBlank(message = "课程名称不能为空")
    private String courseName;

    @ApiModelProperty(value = "课程图片")
    @NotBlank(message = "课程图片不能为空")
    private String courseImage;

    @ApiModelProperty(value = "授课老师ID")
    private Integer courseTeacherId;

    @ApiModelProperty(value = "课程介绍")
    @NotBlank(message = "课程介绍不能为空")
    private String courseIntroduction;

    @ApiModelProperty(value = "授课目标")
    @NotBlank(message = "课程目标不能为空")
    private String courseTarget;

    @ApiModelProperty(value = "是否可见")
    private Boolean visible;

    @ApiModelProperty(value = "教师介绍（图片）")
    private String teacherIntroduction;

    @ApiModelProperty(value = "已参加人数")
    private Integer joinCount;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "课程价格")
    @JsonIgnore
    private Integer coursePrice;

    @ApiModelProperty(value = "课程价格")
    @TableField(select = false, exist = false)
    private BigDecimal price;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Boolean deleted;

    public void setCoursePrice(Integer coursePrice) {
        this.coursePrice = coursePrice;
        if(coursePrice == null)
            this.price = null;
        else{
            int f = coursePrice % 100;
            int d = coursePrice / 100;
            double p = d + f / 100.0;
            this.price = BigDecimal.valueOf(p).setScale(2, BigDecimal.ROUND_DOWN);
//            this.price = ;
        }
    }

    public Integer getCoursePrice() {
        if(coursePrice != null) return coursePrice;
        if(price == null) return null;
        double v = price.doubleValue();
        coursePrice = (int)(v * 100);
        return coursePrice;
    }
}
