package org.fzs.common.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author linjintao
 * @since 2021-03-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="企业活动", description="")
public class EnterpriseActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "活动名称")
    @NotBlank(message = "活动名称不能为空")
    @Excel(name = "活动名称", orderNum = "1", width = 30)
    private String name;

    @ApiModelProperty(value = "活动描述")
    @NotBlank(message = "活动描述不能为空")
    @Excel(name = "活动描述", orderNum = "2", width = 100)
    private String description;

    @ApiModelProperty(value = "企业名称")
    @NotBlank(message = "企业名称不能为空")
    @Excel(name = "企业名称", orderNum = "3", width = 30)
    private String enterpriseName;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    @JsonIgnore
    private Boolean deleted;

    @ApiModelProperty(value = "企业联系方式")
    @Excel(name = "企业联系方式", orderNum = "4", width = 30)
    private String phone;

    @ApiModelProperty(value = "参加人数")
    @Excel(name = "参加人数", orderNum = "5", width = 10)
    private Integer joinCount;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "开始时间", orderNum = "6", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "活动开始时间不能为空")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "结束时间", orderNum = "7", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "活动结束时间不能为空")
    private LocalDateTime endTime;
}
