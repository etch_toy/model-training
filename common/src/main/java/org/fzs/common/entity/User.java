package org.fzs.common.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Data
@Builder
@Accessors(chain = true)
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "id", type = IdType.AUTO)
    @Excel(name = "用户id", orderNum = "0", width = 30)
    private Integer id;

    @ApiModelProperty(value = "用户名")
    @NotBlank(message = "用户名不能为空")
    @Excel(name = "用户名", orderNum = "1", width = 30)
    private String username;

    // 不是所有的修改都需要密码，因此这里允许为空
    @ApiModelProperty(value = "密码")
    @Length(max=20, min = 6, message = "密码长度必须在6-20个字符之间")
    private String password;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty(value = "昵称")
    @Excel(name = "昵称", orderNum = "2", width = 30)
    private String nickname;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "手机号")
//    @Pattern(regexp = "/^((13[0-9])|(17[0-1,6-8])|(15[^4,\\\\D])|(18[0-9]))\\d{8}$/", message = "手机号格式错误")
    @Excel(name = "手机号", orderNum = "3", width = 30)
    private String phone;


    @ApiModelProperty(value = "邮箱")
    @Pattern(regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", message = "邮箱格式错误")
    @Excel(name = "邮箱", orderNum = "4", width = 30)
    private String email;

    @ApiModelProperty(value = "性别")
    @Excel(name = "性别", replace = { "男_0", "女_1", "未知_2" }, orderNum = "5", width = 10)
    private Integer gender;

    @ApiModelProperty(value = "生日")
    @Excel(name = "生日",  format = "yyyy-MM-dd", orderNum = "6", width = 30)
    private LocalDate birthday;

    @ApiModelProperty(value = "年龄")
    @TableField(exist = false, select = false)
    private Integer age;

    @ApiModelProperty(value = "个人描述")
    @Excel(name = "个人描述", orderNum = "7", width = 30)
    private String description;

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
        this.age = ((int)LocalDate.now().until(birthday, ChronoUnit.YEARS));
    }
}