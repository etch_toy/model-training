package org.fzs.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.fzs.common.entity.User;

import java.util.List;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends User {
    @ApiModelProperty(value = "角色Id")
    List<Integer> roleIds;

    @ApiModelProperty(value = "邮箱验证码")
    Integer code;

}
