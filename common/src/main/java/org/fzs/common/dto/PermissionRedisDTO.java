package org.fzs.common.dto;

import lombok.Data;

@Data
public class PermissionRedisDTO {
    String url;
    String roleIdsStr;
    String roleCodesStr;
}
