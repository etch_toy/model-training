package org.fzs.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ResetPassword {
    @ApiModelProperty(value = "新密码")
    @Length(max=20, min = 6, message = "密码长度必须在6-20个字符之间")
    @NotBlank(message = "新密码不能为空")
    String newPassword;
    @ApiModelProperty(value = "邮箱")
    String email;
    @ApiModelProperty(value = "邮箱验证码")
    Integer code;
}
