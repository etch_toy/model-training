package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.CourseContent;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-27
 */
public interface ICourseContentService extends IService<CourseContent> {
    List<CourseContent> findByCourseId(Integer courseId);
}
