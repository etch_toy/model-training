package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.HomePage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-16
 */
public interface IHomePageService extends IService<HomePage> {

}
