package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.Permission;

public interface IPermissionService extends IService<Permission> {

}
