package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.Video;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-25
 */
public interface IVideoService extends IService<Video> {

}
