package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.CourseJoin;

import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
public interface ICourseJoinService extends IService<CourseJoin> {
    List<CourseJoin> getByCourseId(Integer courseId);
    List<CourseJoin> getPassByCourseId(Integer courseId);
    List<CourseJoin> getPassByUserId(Integer userId);
    void joinCourse(Integer userId, Integer courseId);
    void exitCourse(Integer userId, Integer courseId);
    void auditCourseJoin(CourseJoin courseJoin);
}
