package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.Shoot;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-13
 */
public interface IShootService extends IService<Shoot> {

}
