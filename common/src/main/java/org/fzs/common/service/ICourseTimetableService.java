package org.fzs.common.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.CourseTimetable;
import org.fzs.common.vo.CourseTimetableVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
public interface ICourseTimetableService extends IService<CourseTimetable> {
    List<CourseTimetableVo> getShouldNoticeList();
    List<CourseTimetableVo> getVoList(QueryWrapper<CourseTimetable> wrapper);
    CourseTimetableVo getVoById(Integer id);
}
