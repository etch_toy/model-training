package org.fzs.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.dto.UserDTO;
import org.fzs.common.entity.Role;
import org.fzs.common.entity.User;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
public interface IUserService extends IService<User> {
    Integer addUser(UserDTO userDTO);
    void addUserList(List<UserDTO> userDTO);
    IPage<User> getTeachers(Page page, User user);
    IPage<User> getStudents(Page page, User user);
    IPage<User> getAdmins(Page page, User user);
    IPage<User> getByRoleName(Page page, String roleName);
    @Async
    void sendVerifyEmail(String email);
    void verifyEmailCode(String email, Integer code);
    List<Role> getRoles(User user);
}
