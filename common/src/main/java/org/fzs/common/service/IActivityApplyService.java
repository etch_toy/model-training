package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.ActivityApply;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-04-07
 */
public interface IActivityApplyService extends IService<ActivityApply> {

    List<ActivityApply> getByActivityId(Integer activityId);
}
