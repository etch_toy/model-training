package org.fzs.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.dto.NoticeDTO;
import org.fzs.common.entity.CourseTimetable;
import org.fzs.common.entity.Notice;
import org.fzs.common.vo.NoticeVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
public interface INoticeService extends IService<Notice> {
    /**
     * 发送上课通知
     * @param timetableId 课表id
     */
    void sendHasClassNotice(Integer timetableId);

    List<CourseTimetable> getCourseTimetable();

    /**
     * 发送课程公告
     * @param noticeDTO
     */
    void sendCourseAnnouncement(NoticeDTO noticeDTO);


    /**
     * 发送企业通知公告
     * @param noticeDTO
     */
    void sendActivityAnnouncement(NoticeDTO noticeDTO);

    /**
     * 发送系统通知
     * @param noticeDTO
     */
    void sendSystemNotice(NoticeDTO noticeDTO);

    /**
     * 通过用户id跟courseId进行过滤通知。
     * @param userId 用户id，必须。用于获取该用户接受的通知
     * @param courseId 课程id。可以获取某个课程的公告。
     * @return
     */
    IPage<NoticeVo> getByUserIdAndCourseId(Integer userId, Integer courseId, Integer type, Page<NoticeVo> page);

    Integer notReadCount(Integer userId);
}
