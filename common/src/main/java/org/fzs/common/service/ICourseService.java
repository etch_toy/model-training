package org.fzs.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.Course;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
public interface ICourseService extends IService<Course> {
    List<Course> getByTeacherId(Integer userId, Boolean visible);
    void descJoinCount(Integer courseId);
    void incrJoinCount(Integer courseId);
    void hiddenByTeacherId(Integer teacherId);
}
