package org.fzs.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.fzs.common.entity.EnterpriseActivity;
import org.fzs.common.vo.EnterpriseActivityVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-14
 */

public interface IEnterpriseActivityService extends IService<EnterpriseActivity> {

    IPage<EnterpriseActivityVo> getPage(EnterpriseActivityVo vo, Page page);
    IPage<EnterpriseActivityVo> getPageByAdmin(EnterpriseActivityVo vo, Page page);
    void descJoinCount(Integer id);
    void incrJoinCount(Integer id);
}
