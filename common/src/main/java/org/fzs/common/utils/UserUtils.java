package org.fzs.common.utils;

import org.fzs.common.entity.User;
import org.springframework.security.core.Authentication;

public class UserUtils {
    public static User getLoginUser(Authentication authentication){
        if(authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof User) {
                return (User) principal;
            }
        }
        // todo 先暂时使用一个用户来测试
        return null;
    }
}
