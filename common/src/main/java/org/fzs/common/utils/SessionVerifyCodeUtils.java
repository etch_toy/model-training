//package org.fzs.common.utils;
//
//
//import org.apache.commons.lang3.StringUtils;
//
//import java.util.concurrent.ConcurrentHashMap;
//
//public class SessionVerifyCodeUtils {
//    private static final ConcurrentHashMap<String, String> SESSION_CODE_MAP = new ConcurrentHashMap<>();
//    private static final ThreadLocal<String> SESSION_ID_MAP = new ThreadLocal<>();
//
//    public static void setVerifyCode(String code){
//        if(!StringUtils.isBlank(code)) {
//            String sessionId = SESSION_ID_MAP.get();
//            if(sessionId == null) return;
//            SESSION_CODE_MAP.put(sessionId, code);
//        }
//    }
//
//    public static String getVerifyCode(){
//        String sessionId = SESSION_ID_MAP.get();
//        if(sessionId == null) return null;
//        return SESSION_CODE_MAP.get(sessionId);
//    }
//
//    public static void setSessionId(String sessionId){
//        if(!StringUtils.isBlank(sessionId)) {
//            SESSION_ID_MAP.set(sessionId);
//        }
//    }
//
//    public static String getSessionId(){
//        return SESSION_ID_MAP.get();
//    }
//}
