//package org.org.fzs.common.utils;
//
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//import java.io.Serializable;
//
//@Data
//@ApiModel
//public class BaseResult implements Serializable {
//    @ApiModelProperty(value = "请求结果", example = "0-失败，1-成功")
//    private Integer result;
//    private Object data;
//    @ApiModelProperty(value = "消息码", example = "参考HTTP状态码")
//    private Integer msgCode;
//    private String msg;
//    private String token;
//
//    public static final Integer RESULT_SUCCESS = 1;
//    public static final Integer RESULT_FAIL = 0;
//    public static final String MSG_SUCCESS = "操作成功";
//    public static final Integer MSG_CODE_SUCCESS = 200;
//
//    public static final Integer MSG_CODE_INTERNAL_ERROR = 500;
//    public static final Integer MSG_CODE_NOT_FOUND = 404;
//    public static final Integer MSG_CODE_UNAUTHORIZED = 401;
//    public static final Integer MSG_CODE_NOT_ACCEPTABLE = 406;
//    public static final Integer MSG_CODE_BAD_ARGUMENT = 400;
//
//    private static BaseResult createResult(Integer result, Object data, String msg, Integer msgCode, String token) {
//        BaseResult baseResult = new BaseResult();
//
//        baseResult.setResult(result);
//        baseResult.setData(data);
//        baseResult.setMsg(msg);
//        baseResult.setMsgCode(msgCode);
//        baseResult.setToken(token);
//
//        return baseResult;
//    }
//
//    public static BaseResult successWithToken(Object data, String token) {
//        return createResult(RESULT_SUCCESS, data, MSG_SUCCESS, 200, token);
//    }
//
//    public static BaseResult successWithToken(String token) {
//        return createResult(RESULT_SUCCESS, null, MSG_SUCCESS, 200, token);
//    }
//
//    public static BaseResult success(Object data) {
//        return createResult(RESULT_SUCCESS, data, MSG_SUCCESS, 200,null);
//    }
//
//    public static BaseResult success() {
//        return createResult(RESULT_SUCCESS, null, MSG_SUCCESS, 200, null);
//    }
//
//    public static BaseResult fail(String msg, Integer msgCode) {
//        return createResult(RESULT_FAIL, null, msg, msgCode, null);
//    }
//
//    public static BaseResult loginError(String msg) {
//        return createResult(RESULT_FAIL, null, msg, MSG_CODE_UNAUTHORIZED, null);
//    }
//
//    public static BaseResult internalError(String msg) {
//        return createResult(RESULT_FAIL, null, msg, MSG_CODE_INTERNAL_ERROR, null);
//    }
//
//    public static BaseResult badArgument(String msg) {
//        return createResult(RESULT_FAIL, null, msg, MSG_CODE_BAD_ARGUMENT, null);
//    }
//}
