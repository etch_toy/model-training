package org.fzs.common.utils;
import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static String RESOURCE_ROLES_KEY = "auth_permission";
    public static final String AUTHORITY_PREFIX = "ROLE_";
    public static final String AUTHORITY_CLAIM_NAME = "authorities";
    public static final String EMAIL_CODE_PRE = "email_code_";
    public static final List<String> SWAGGER_PATHS = new ArrayList<>();
    static {
        SWAGGER_PATHS.add("/swagger-ui.html");
        SWAGGER_PATHS.add("/swagger-resources/**");
        SWAGGER_PATHS.add("/webjars/**");
        SWAGGER_PATHS.add("/*/api-docs");
    }
    public static final String VIDEO_UPLOAD_KEY_PRE = "video_key#";
}
