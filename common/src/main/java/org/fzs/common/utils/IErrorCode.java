package org.fzs.common.utils;


public interface IErrorCode {
    long getCode();

    String getMessage();
}
