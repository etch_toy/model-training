package org.fzs;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EnableDubbo
@EnableWebSecurity
@EnableAspectJAutoProxy
@SpringBootApplication(exclude = {FeignAutoConfiguration.class})
public class CourseApplication {
//    @PostConstruct
//    void init(){
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//    }
    public static void main(String[] args) {
        SpringApplication.run(CourseApplication.class, args);
    }
}
