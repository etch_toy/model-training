package org.fzs.run;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.rpc.RpcException;
import org.fzs.common.service.ICourseContentService;
import org.fzs.common.service.ICourseService;
import org.fzs.common.service.ICourseTimetableService;
import org.fzs.common.service.IUserService;
import org.fzs.mapper.CourseMapper;
import org.fzs.service.NoticeTimerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

@Component
@RequiredArgsConstructor
@Slf4j
public class NoticeHasClassRunner implements ApplicationRunner {
    @Autowired
    private NoticeTimerService noticeTimerService;
    @Autowired
    RequestMappingHandlerMapping requestMappingHandlerMapping;
    private final ICourseService courseService;
    private final ICourseContentService courseContentService;
    private final CourseMapper courseMapper;
    private final ICourseTimetableService courseTimetableService;
    @DubboReference
    private IUserService userService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        int retry = 3;
        while(retry > 0) {
            try {
                noticeTimerService.startTimer();
                break;
            } catch (RpcException e) {
                log.warn("运行启动任务失败，正在进行重试");
                Thread thread = Thread.currentThread();
                LocalDateTime end = LocalDateTime.now().plusSeconds(10);
                while(LocalDateTime.now().isBefore(end)) {
                    thread.yield();
                }
                --retry;
                if(retry == 0) throw e;
            }
        }
        System.out.println("---");
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
        Map<String, Predicate<Class<?>>> pathPrefixes = requestMappingHandlerMapping.getPathPrefixes();
        Set<String> keySet = pathPrefixes.keySet();
        for (RequestMappingInfo rmi : handlerMethods.keySet()) {
            PatternsRequestCondition path = rmi.getPatternsCondition();
            System.out.println(path);
//            if (handlerMethod.hasMethodAnnotation(NoLogin.class)) {
//                PatternsRequestCondition prc = rmi.getPatternsCondition();
//                Set<String> patterns = prc.getPatterns();
//                noLoginUrlSet.addAll(patterns);
//            }
        }
    }
}
