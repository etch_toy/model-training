package org.fzs.handler;

import org.fzs.common.handler.BaseMybatisPlusMetaObjectHandler;
import org.springframework.stereotype.Component;

/**
 * mybatis-plus字段处理
 */
@Component
public class MybatisPlusMetaObjectHandler extends BaseMybatisPlusMetaObjectHandler {
}
