package org.fzs.service;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.fzs.common.entity.CourseTimetable;
import org.fzs.common.service.ICourseTimetableService;
import org.fzs.common.service.INoticeService;
import org.fzs.common.vo.CourseTimetableVo;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class NoticeTimerService {
    private final HashedWheelTimer timer = new HashedWheelTimer(20, TimeUnit.SECONDS);
    private final Map<Integer, Timeout> timeoutMap = new ConcurrentHashMap<>();
    private final INoticeService noticeService;
    private final ICourseTimetableService courseTimetableService;

    @Data
    public class MyTask implements TimerTask{
        private Integer timetableId;
        private LocalTime classTime;
        // 需要通知的周几，1-7代表周一到周日
        private List<Integer> day;

        @Override
        public void run(Timeout timeout) throws Exception {
            TimerTask task = timeout.task();
            if(task instanceof  MyTask){
                MyTask myTask =  (MyTask)task;
                // 如果不存在，不执行逻辑
                Integer id = myTask.getTimetableId();
                Timeout timeout2 = timeoutMap.get(id);
                if(timeout2 == null || timeout2 != timeout) return;
                CourseTimetable courseTimetable = courseTimetableService.getById(timetableId);
                // 如果已经结课，不再通知。
                if(handlerEndCourse(courseTimetable)) return;
                // 到了开始时间才通知
                if(courseTimetable.getStartTime() == null ||
                        courseTimetable.getStartTime().isBefore(LocalDateTime.now())) {
                    // 发送课程通知
                    noticeService.sendHasClassNotice(id);
                }
                // 获取距离下次通知的时间
                long minutes = myTask.getNextNoticeMinute();
                if(minutes <= 0) return;
                else if(minutes == 0) minutes = 1440;
                Timeout timeout1 = timer.newTimeout(this, minutes, TimeUnit.MINUTES);
                timeoutMap.put(id, timeout1);
            }
        }

        /**
         * 获取距离下次通知的分钟数
         * @return
         */
        public long getNextNoticeMinute(){
            LocalTime classTime = this.getClassTime();
            List<Integer> dayList = this.getDay();
            return getNextMinute(classTime, dayList);
        }

    }

    public void startTimer(){
        List<CourseTimetableVo> list = courseTimetableService.getShouldNoticeList();
        list.forEach(this::addTimetableNotice);
        timer.start();
    }

    /**
     * 加入定时通知到定时器中
     */
    public void addTimetableNotice(CourseTimetableVo courseTimetable){
        MyTask myTask = new MyTask();
        myTask.setClassTime(courseTimetable.getClassTime());
        myTask.setTimetableId(courseTimetable.getId());
        myTask.setDay(courseTimetable.getWeekDayList());
        // 提前1小时提醒. 如果已经过了这个时间，那就明天这个时间再通知。1440分钟是一天时间
        long minutes = getNextMinute(courseTimetable.getClassTime(), courseTimetable.getWeekDayList());
        if(minutes == -1) return;
        else if(minutes == 0) minutes = 1440;
        Timeout timeout = timer.newTimeout(myTask, minutes, TimeUnit.MINUTES);
        timeoutMap.put(courseTimetable.getId(), timeout);
    }

    /**
     * 获取距离下次通知的分钟数
     */
    private long getNextMinute(LocalTime classTime, List<Integer> dayList){
        if(classTime == null) return -1;
        // 跳过不通知的星期几
        if(CollectionUtils.isEmpty(dayList)) return -1;
        LocalTime now = LocalTime.now();
        LocalDateTime nowDate = LocalDateTime.now();
        int nowDay = nowDate.getDayOfWeek().getValue();
        // 如果是今天时间还没到，且今天需要通知，那就在今天续约就行了。提前10分钟通知
        if(now.isBefore(classTime.minusMinutes(60)) && dayList.contains(nowDay))
            return Duration.between(now, classTime.minusMinutes(60)).toMinutes();
        // 如果今天这个时间已经过了
        long minutes = -1;
        for (Integer day : dayList) {
            if(day > nowDay){
                minutes = (day - nowDay) * 1440;
                break;
            }
        }
        // 如果遍历完早不到，那肯定是下周再通知了
        if(minutes == -1) {
            Integer nextDay = dayList.get(0);
            // +7 的话就是下周的时间了，1440是一天的时间
            minutes = (nextDay + 7 - nowDay) * 1440;
        }
        // 今天的时间误差（负数）
        long nowDateMinutes = Duration.between(now, classTime).toMinutes();
        // 提前1小时提醒。
        nowDateMinutes -= 60;
        return minutes + nowDateMinutes;
    }

    /**
     * 取消课表定时通知
     * @param timetableId 课表id
     * @return 是否成功取消，不存在返回false
     */
    public void cancelByTimetableId(Integer timetableId){
        Timeout timeout = timeoutMap.get(timetableId);
        if(timeout == null) return;
        timeoutMap.remove(timetableId);
        timeout.cancel();
    }

    /**
     * 已经结课的课程不再通知
     */
    public boolean handlerEndCourse(CourseTimetable courseTimetable){
        if(courseTimetable == null ||
                courseTimetable.getEndTime() != null && courseTimetable.getEndTime().isBefore(LocalDateTime.now())){
            cancelByTimetableId(courseTimetable.getId());
            return true;
        }
        return false;
    }





    public static void main(String[] args) throws InterruptedException {
//        NoticeTimerService org.fzs.service = new NoticeTimerService();
//        org.fzs.service.startTimer();
//        TimeUnit.SECONDS.sleep(20);
//        System.out.println("取消1");
//        org.fzs.service.cancelByTimetableId(1);
//        HashedWheelTimer timer = new HashedWheelTimer();
//        TimerTask task1 = new TimerTask() {
//            public void run(Timeout timeout) throws Exception {
//                System.out.println("------------");
//                timer.newTimeout(this, 2, TimeUnit.SECONDS);
//            }
//        };
//        timer.newTimeout(task1, 2, TimeUnit.SECONDS);
//        timer.start();
//        Timeout timeout = timer.newTimeout((t) -> {
//            new Thread(){
//                @Override
//                public void run() {
//                    System.out.println( " executed");
//                    try {
//                        TimeUnit.SECONDS.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }.start();
//        }, 5, TimeUnit.SECONDS);

        try {
            TimeUnit.SECONDS.sleep(60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
