package org.fzs.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.fzs.common.entity.CourseTimetable;
import org.fzs.common.service.ICourseTimetableService;
import org.fzs.common.vo.CourseTimetableVo;
import org.fzs.mapper.CourseTimetableMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Service
@Component
public class CourseTimetableServiceImpl extends ServiceImpl<CourseTimetableMapper, CourseTimetable>
        implements ICourseTimetableService {

    @Override
    public List<CourseTimetableVo> getShouldNoticeList() {
        QueryWrapper<CourseTimetable> wrapper = new QueryWrapper<>();
        wrapper.gt("end_time", LocalDateTime.now());
        return this.getVoList(wrapper);
    }

    @Override
    public List<CourseTimetableVo> getVoList(QueryWrapper<CourseTimetable> wrapper) {
        List<CourseTimetable> list = this.list(wrapper);
        return list.stream().map(courseTimetable -> {
            CourseTimetableVo vo = new CourseTimetableVo();
            BeanUtils.copyProperties(courseTimetable, vo);
            List<Integer> days = JSONArray.parseArray(courseTimetable.getWeekDays(), Integer.class);
            vo.setWeekDayList(days);
            return vo;
        }).collect(Collectors.toList());
    }

    @Override
    public CourseTimetableVo getVoById(Integer id) {
        CourseTimetable courseTimetable = this.getById(id);
        if(courseTimetable == null) return null;
        CourseTimetableVo vo = new CourseTimetableVo();
        BeanUtils.copyProperties(courseTimetable, vo);
        List<Integer> days = JSONArray.parseArray(courseTimetable.getWeekDays(), Integer.class);
        vo.setWeekDayList(days);
        return vo;
    }


}
