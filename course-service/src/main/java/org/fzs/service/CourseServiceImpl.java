package org.fzs.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.Service;
import org.fzs.common.entity.Course;
import org.fzs.common.service.ICourseService;
import org.fzs.mapper.CourseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Service
@Component
@RequiredArgsConstructor
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    private final CourseMapper courseMapper;

    @Override
    public List<Course> getByTeacherId(Integer userId, Boolean visible) {
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        wrapper.eq("course_teacher_id", userId);
        if(visible != null)
            wrapper.eq("visible", visible);
        return this.list(wrapper);
    }

    @Override
    public void descJoinCount(Integer courseId) {
        courseMapper.plusJoinCount(courseId, -1);
    }

    @Override
    public void incrJoinCount(Integer courseId) {
        courseMapper.plusJoinCount(courseId, 1);
    }

    @Override
    public void hiddenByTeacherId(Integer teacherId) {
        UpdateWrapper<Course> wrapper = new UpdateWrapper<>();
        wrapper.set("visible", false).eq("teacher", teacherId);
        this.update(wrapper);
    }

}
