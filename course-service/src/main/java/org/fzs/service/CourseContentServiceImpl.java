package org.fzs.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.fzs.common.entity.CourseContent;
import org.fzs.common.service.ICourseContentService;
import org.fzs.mapper.CourseContentMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-27
 */
@Service
public class CourseContentServiceImpl extends ServiceImpl<CourseContentMapper, CourseContent> implements ICourseContentService {

    @Override
    public List<CourseContent> findByCourseId(Integer courseId) {
        QueryWrapper<CourseContent> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        return this.list(wrapper);
    }
}
