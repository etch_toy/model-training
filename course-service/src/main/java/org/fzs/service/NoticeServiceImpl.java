package org.fzs.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.fzs.common.dto.NoticeDTO;
import org.fzs.common.entity.*;
import org.fzs.common.exception.CustomException;
import org.fzs.common.service.*;
import org.fzs.common.vo.NoticeVo;
import org.fzs.mapper.NoticeMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Service
@Component
@RequiredArgsConstructor
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {
    private final NoticeMapper noticeMapper;
    private final INoticeBoxService noticeBoxService;
    private final ExecutorService pool = Executors.newFixedThreadPool(3);
    private final ICourseService courseService;
    private final ICourseJoinService iCourseJoinService;
    private final ICourseTimetableService courseTimetableService;
    @Reference
    IUserService userService;
    @Reference
    IActivityApplyService activityApplyService;
    @Reference
    IEnterpriseActivityService enterpriseActivityService;



    @Override
    public void sendHasClassNotice(Integer timetableId) {

        CourseTimetable timetable = courseTimetableService.getById(timetableId);
        if(timetable == null) return;
        Course course = courseService.getById(timetable.getCourseId());
        String courseName = course.getCourseName();
        Notice notice = Notice.builder()
                .noticeTitle("课程" + courseName + "准备上课了")
                .noticeContent(courseName + "将在" +
                        timetable.getClassTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " 开始上课")
                .type(Notice.TYPE_HAS_CLASS_NOTICE)
                .build();
        this.save(notice);
        this.sendCourseNoticeToUser(notice.getId(), course.getId());
        this.sendCourseNoticeToTeacher(notice.getId(), course.getCourseTeacherId());
    }

    @Override
    public List<CourseTimetable> getCourseTimetable() {
        return courseTimetableService.list();
    }

    @Override
    public void sendCourseAnnouncement(NoticeDTO noticeDTO) {
        Course course = courseService.getById(noticeDTO.getCourseId());
        if(course == null) throw new CustomException("此课程不存在");
        Notice notice = Notice.builder()
                .noticeTitle(noticeDTO.getTitle())
                .noticeContent(noticeDTO.getContent())
                .courseId(noticeDTO.getCourseId())
                .type(Notice.TYPE_COURSE_ANNOUNCEMENT)
                .build();
        this.save(notice);
        sendCourseNoticeToUser(notice.getId(), course.getId());
    }

    private void sendCourseNoticeToUser(Integer noticeId, Integer courseId){
        pool.execute(() ->{
            List<CourseJoin> courseJoins = iCourseJoinService.getPassByCourseId(courseId);
            List<NoticeBox> list = courseJoins.stream()
                    .map(item -> NoticeBox.builder().isRead(false)
                            .noticeId(noticeId).userId(item.getUserId()).build())
                    .collect(Collectors.toList());
            noticeBoxService.saveBatch(list);
        });
    }

    @Override
    public void sendActivityAnnouncement(NoticeDTO noticeDTO) {
        EnterpriseActivity ea = enterpriseActivityService.getById(noticeDTO.getCourseId());
        if(ea == null) throw new CustomException("此活动不存在");
        Notice notice = Notice.builder()
                .noticeTitle(noticeDTO.getTitle())
                .noticeContent(noticeDTO.getContent())
                .courseId(noticeDTO.getCourseId())
                .type(Notice.TYPE_ACTIVITY_ANNOUNCEMENT)
                .build();

        this.save(notice);
        sendActivityNoticeToUser(notice.getId(), ea.getId());
    }

    @Override
    public void sendSystemNotice(NoticeDTO noticeDTO) {
        Notice notice = Notice.builder()
                .noticeTitle(noticeDTO.getTitle())
                .noticeContent(noticeDTO.getContent())
                .courseId(noticeDTO.getCourseId())
                .type(Notice.TYPE_SYSTEM_NOTICE)
                .build();
        this.save(notice);
        sendNoticeToAllUser(notice.getId());
    }



    private void sendActivityNoticeToUser(Integer noticeId, Integer activityId){
        pool.execute(() ->{
            List<ActivityApply> applyList = activityApplyService.getByActivityId(activityId);
            List<NoticeBox> list = applyList.stream()
                    .map(item -> NoticeBox.builder().isRead(false)
                            .noticeId(noticeId).userId(item.getUserId()).build())
                    .collect(Collectors.toList());
            noticeBoxService.saveBatch(list);
        });
    }

    private void sendCourseNoticeToTeacher(Integer noticeId, Integer userId){
        NoticeBox noticeBox = NoticeBox.builder().isRead(false)
                .noticeId(noticeId).userId(userId).build();
        noticeBoxService.save(noticeBox);
    }

    private void sendNoticeToAllUser(Integer noticeId){
        pool.execute(() ->{
            List<User> users = userService.list();
            List<NoticeBox> list = users.stream()
                    .map(user -> NoticeBox.builder().isRead(false)
                            .noticeId(noticeId).userId(user.getId()).build())
                    .collect(Collectors.toList());
            noticeBoxService.saveBatch(list);
        });

    }


    @Override
    public IPage<NoticeVo> getByUserIdAndCourseId(Integer userId, Integer courseId,Integer type,  Page<NoticeVo> page){
        IPage<NoticeVo> result = noticeMapper.queryByUserIdAndCourseId(userId, courseId, type, page);
        Set<Integer> courseIds = result.getRecords().stream()
                .filter(item -> Objects.nonNull(item.getCourseId()))
                .map(NoticeVo::getCourseId)
                .collect(Collectors.toSet());
        if(!CollectionUtils.isEmpty(courseIds)) {
            List<Course> courses = courseService.listByIds(courseIds);
            Map<Integer, Course> courseIdToEntity =
                    courses.stream().collect(Collectors.toMap(Course::getId, course -> course));
            // 拼接参数
            List<NoticeVo> resultList = result.getRecords().stream().map(notice -> {
                if (notice.getCourseId() == null) return notice;
                Course course = courseIdToEntity.getOrDefault(notice.getCourseId(),
                        Course.builder().courseName("已删除的课程").build());
                NoticeVo vo = new NoticeVo();
                BeanUtils.copyProperties(notice, vo);
                vo.setCourseName(course.getCourseName());
                return vo;
            }).collect(Collectors.toList());
            result.setRecords(resultList);
        }
        return result;
    }

    @Override
    public Integer notReadCount(Integer userId) {
        QueryWrapper<NoticeBox> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId)
                .eq("is_read", false);
        return noticeBoxService.count(wrapper);
    }
}
