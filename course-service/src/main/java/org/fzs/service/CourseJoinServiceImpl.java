package org.fzs.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.Service;
import org.fzs.common.entity.CourseJoin;
import org.fzs.common.service.ICourseJoinService;
import org.fzs.common.service.ICourseService;
import org.fzs.mapper.CourseJoinMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Service
@Component
@RequiredArgsConstructor
public class CourseJoinServiceImpl extends ServiceImpl<CourseJoinMapper, CourseJoin> implements ICourseJoinService {

    private final CourseJoinMapper mapper;
    private final ICourseService courseService;

    @Override
    public List<CourseJoin> getByCourseId(Integer courseId) {
        QueryWrapper<CourseJoin> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId).orderByDesc("update_time");
        return this.list(wrapper);
    }

    @Override
    public List<CourseJoin> getPassByCourseId(Integer courseId) {
        QueryWrapper<CourseJoin> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId)
                .eq("is_pass",true)
                .orderByDesc("update_time");;
        return this.list(wrapper);
    }

    @Override
    public List<CourseJoin> getPassByUserId(Integer userId) {
        QueryWrapper<CourseJoin> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId)
                .eq("is_pass",true)
                .orderByDesc("update_time");;
        return this.list(wrapper);
    }

    @Override
    @Transactional
    public void joinCourse(Integer userId, Integer courseId) {
        QueryWrapper<CourseJoin> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId)
                .eq("user_id", userId)
                .and( item -> item.eq("is_pass", true)
                        .or().isNull("is_pass"));
        List<CourseJoin> list = mapper.selectList(wrapper);
        if(!CollectionUtils.isEmpty(list)) return;
        int count = mapper.insert(CourseJoin.builder().
                courseId(courseId).userId(userId).isPass(null).build());
//        iCourseJoinService.remove(wrapper);
    }

    @Override
    @Transactional
    public void exitCourse(Integer userId, Integer courseId) {
        QueryWrapper<CourseJoin> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId)
                .eq("user_id", userId);
        List<CourseJoin> list = mapper.selectList(wrapper);
        if(CollectionUtils.isEmpty(list)) return;
        CourseJoin one = list.get(0);
        int count = mapper.delete(wrapper);
        int num = 0;
        for (CourseJoin join : list) {
            if(join.getIsPass()) num++;
        }
        // 如果是已经参加的，才减少
        if(num > 0 && count > 0) {
            for (int i = 0; i < num; i++)
                courseService.descJoinCount(courseId);
        }
    }

    @Override
    @Transactional
    public void auditCourseJoin(CourseJoin courseJoin) {
        QueryWrapper<CourseJoin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseJoin.getCourseId())
                .eq("user_id", courseJoin.getUserId())
                .and( item -> item.eq("is_pass", true)
                        .or().isNull("is_pass"))
                .orderByDesc("create_time");
        CourseJoin one = this.getOne(queryWrapper, false);
        // 没有修改的就不管
        if(one == null) return;
        else if(one.getIsPass() == null && courseJoin.getIsPass() == null) return;
        else if(one.getIsPass() == courseJoin.getIsPass()) return;
        one.setIsPass(courseJoin.getIsPass());
        boolean success = this.updateById(one);
        if(success && one.getIsPass()) courseService.incrJoinCount(courseJoin.getCourseId());
    }
}
