package org.fzs.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.fzs.common.entity.NoticeBox;
import org.fzs.common.service.INoticeBoxService;
import org.fzs.mapper.NoticeBoxMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Service
public class NoticeBoxServiceImpl extends ServiceImpl<NoticeBoxMapper, NoticeBox> implements INoticeBoxService {

}
