package org.fzs.filter;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.fzs.common.entity.User;
import org.fzs.common.utils.Constants;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(0)
@WebFilter(filterName = "JwtAuthenticationTokenFilter", urlPatterns = "/*")
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // 放行swagger
        PathMatcher pathMatcher = new AntPathMatcher();
        for (String swaggerPath : Constants.SWAGGER_PATHS) {
            if(pathMatcher.match(swaggerPath, request.getRequestURI())) {
                filterChain.doFilter(request, response);
                return;
            }
        }
        User user;
        JSONArray authoritiesArray;
        try {
            String userStr = request.getHeader("user");
            JSONObject jsonObject = JSONObject.parseObject(userStr);
            String userJson = jsonObject.getString("user_name");
            user = JSONObject.parseObject(userJson, User.class);
            //用户权限
            authoritiesArray = jsonObject.getJSONArray("authorities");
        }catch (Exception e){
//            response.setCharacterEncoding("utf-8");
//            response.setContentType("application/json");
//            response.getWriter().write(JSONObject.toJSONString(CommonResult.validateFailed("token 内容错误!")));

            filterChain.doFilter(request,response);
            return;
        }
//        String json = EncryptUtil.decodeUTF8StringBase64(token);
        //将token转成json对象
//        JSONObject jsonObject = JSON.parseObject(json);
        //用户身份信息
//            UserDTO userDTO = new UserDTO();
//            String principal = jsonObject.getString("principal");
//            userDTO.setUsername(principal);
//        User userDTO = JSON.parseObject(jsonObject.getString("principal"), User.class);
        String[] authorities;
        if(authoritiesArray != null && authoritiesArray.size() == 0) {
            authorities = authoritiesArray.toArray(new String[authoritiesArray.size()]);
        }else {
            String[] strings = new String[1];
            strings[0] = "4";
            authorities = strings;
        }
        //将用户信息和权限填充 到用户身份token对象中
        UsernamePasswordAuthenticationToken authenticationToken
                = new UsernamePasswordAuthenticationToken(user,null, AuthorityUtils.createAuthorityList(authorities));
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        Authentication authenticate = new NoAuthAuthenticationProvider().authenticate(authenticationToken);
        //将authenticationToken填充到安全上下文
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        System.out.println("身份：" + authorities);
        filterChain.doFilter(request,response);
    }

    public static class NoAuthAuthenticationProvider implements AuthenticationProvider{

        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            return authentication;
        }

        @Override
        public boolean supports(Class<?> aClass) {
            return true;
        }
    }
}
