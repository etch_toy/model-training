package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.fzs.common.entity.CourseTimetable;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Mapper
public interface CourseTimetableMapper extends BaseMapper<CourseTimetable> {

}
