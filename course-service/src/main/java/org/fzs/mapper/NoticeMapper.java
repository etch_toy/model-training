package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.fzs.common.entity.Notice;
import org.fzs.common.vo.NoticeVo;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {
    @Select("<script> select n.*, b.is_read from notice n, notice_box b where " +
            "n.deleted = 0 and b.deleted = 0 and n.id = b.notice_id" +
            " and b.user_id = #{userId} <if test = 'courseId != null'> and n.course_id = #{courseId} </if> " +
            "<if test = 'type != null'> and n.type = #{type} </if>" +
            "order by n.update_time desc</script>")
    IPage<NoticeVo> queryByUserIdAndCourseId(@Param("userId") Integer userId, @Param("courseId") Integer courseId,
                                             @Param("type") Integer type,
                                             @Param("page") Page<NoticeVo> page);
}
