package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.fzs.common.entity.Course;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@Mapper
public interface CourseMapper extends BaseMapper<Course> {

    @Update("update course set join_count = join_count + #{num} where id = #{id} ")
    void plusJoinCount(@Param("id") Integer id, @Param("num") Integer num);
}
