package org.fzs.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.fzs.common.dto.NoticeDTO;
import org.fzs.common.entity.Notice;
import org.fzs.common.entity.NoticeBox;
import org.fzs.common.entity.User;
import org.fzs.common.service.INoticeBoxService;
import org.fzs.common.service.INoticeService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.UserUtils;
import org.fzs.common.vo.NoticeVo;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/notice")
public class NoticeController {
    private final INoticeService noticeService;
    private final INoticeBoxService iNoticeBoxService;

    @GetMapping("/page")
    public CommonResult page(@RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                            @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                            @RequestParam(required = false) Integer courseId,
                                              @RequestParam(required = false) Integer type,
                                            Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        Page<NoticeVo> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);

        return CommonResult.success(noticeService.getByUserIdAndCourseId(user.getId(), courseId, type,  page));
    }

    @GetMapping("getAllNotice")
    public CommonResult<IPage<Notice>> getAllNotice(@RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                              @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                              @RequestParam(required = false) Integer courseId,
                                              @RequestParam(required = false) Integer type,
                                              Authentication authentication){
        Page<Notice> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        QueryWrapper<Notice> wrapper = new QueryWrapper<>();
        if(courseId != null)
            wrapper.eq("course_id", courseId);
        if(type != null)
            wrapper.eq("type", type);
        wrapper.orderByDesc("update_time");
        return CommonResult.success(noticeService.page(page, wrapper));
    }

    @PostMapping("/sendSystemNotice")
    public CommonResult<Notice> sendSystemNotice(@RequestBody @Validated NoticeDTO noticeDTO, BindingResult bindingResult){
        noticeService.sendSystemNotice(noticeDTO);
        return CommonResult.success();
    }

    @PostMapping("/sendCourseAnnouncement")
    public CommonResult<Notice> sendCourseAnnouncement(@RequestBody @Validated NoticeDTO noticeDTO, BindingResult bindingResult){
        if(noticeDTO.getCourseId() == null) return CommonResult.badArgument("课程id不能为空");
        noticeService.sendCourseAnnouncement(noticeDTO);
        return CommonResult.success();
    }

    @PostMapping("/sendActivityAnnouncement")
    public CommonResult<Notice> sendActivityAnnouncement(@RequestBody @Validated NoticeDTO noticeDTO, BindingResult bindingResult){
        if(noticeDTO.getCourseId() == null) return CommonResult.badArgument("活动id不能为空");
        noticeService.sendActivityAnnouncement(noticeDTO);
        return CommonResult.success();
    }

    @PostMapping("/setRead/{id}")
    public CommonResult setRead(@PathVariable Integer id, @ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        UpdateWrapper<NoticeBox> wrapper = new UpdateWrapper<>();
        wrapper.set("is_read", true);
        wrapper.eq("notice_id", id)
                .eq("user_id", user.getId());
        iNoticeBoxService.update(wrapper);
        return CommonResult.success();
    }

    @GetMapping("/notReadCount")
    public CommonResult notReadCount(Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        return CommonResult.success(noticeService.notReadCount(user.getId()));
    }


    @GetMapping("/systemNotices")
    public CommonResult systemNotices(@RequestParam(required = false, defaultValue = "1") Integer pageNum,
                             @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                             Authentication authentication){
        Page<Notice> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        QueryWrapper<Notice> wrapper = new QueryWrapper<>();
        wrapper.eq("type", Notice.TYPE_SYSTEM_NOTICE);
        return CommonResult.success(noticeService.page(page, wrapper));
    }
}
