package org.fzs.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.fzs.common.entity.Course;
import org.fzs.common.entity.CourseContent;
import org.fzs.common.entity.User;
import org.fzs.common.service.ICourseContentService;
import org.fzs.common.service.ICourseService;
import org.fzs.common.service.ICourseTimetableService;
import org.fzs.common.service.IUserService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.UserUtils;
import org.fzs.common.vo.CourseVo;
import org.fzs.mapper.CourseMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/course")
public class CourseController {
    private final ICourseService courseService;
    private final ICourseContentService courseContentService;
    private final CourseMapper courseMapper;
    private final ICourseTimetableService courseTimetableService;
    @DubboReference
    private IUserService userService;


    @ApiOperation("课程详情")
    @GetMapping("{id}")
    public CommonResult<Course> detail(@PathVariable Integer id){
        List<CourseContent> contents = courseContentService.findByCourseId(id);
        Course course = courseService.getById(id);
        CourseVo vo = new CourseVo();
        BeanUtils.copyProperties(course, vo);
        vo.setContentList(contents);
        vo.setContentCount(contents.size());
        return CommonResult.success(vo);
    }

    @ApiOperation("章节详情")
    @GetMapping("content/{id}")
    public CommonResult<CourseContent> content(@PathVariable Integer id){
        return CommonResult.success(courseContentService.getById(id));
    }

    @ApiOperation("新增或者修改章节")
    @PostMapping("content/saveOrUpdate")
    public CommonResult<Integer> saveOrUpdateContent(@Validated @RequestBody CourseContent content,
                                              @ApiIgnore BindingResult bindingResult){
        courseContentService.saveOrUpdate(content);
        return CommonResult.success(content.getId());
    }

    @ApiOperation("删除章节")
    @DeleteMapping("content/{id}")
    public CommonResult<Integer> deleteContent(@PathVariable Integer id){
        courseContentService.removeById(id);
        return CommonResult.success();
    }

    @ApiOperation("新增或者修改课程")
    @PostMapping("save")
    public CommonResult save(@Validated @RequestBody Course course, @ApiIgnore Authentication authentication,
                                              @ApiIgnore BindingResult bindingResult){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        if(course.getCourseTeacherId() == null) {
            course.setCourseTeacherId(user.getId());
        }
        User teacher = userService.getById(course.getCourseTeacherId());
        String description = teacher.getDescription();
        course.setTeacherIntroduction(description);

        course.setId(null);
        courseService.save(course);
        return CommonResult.success(course.getId());
    }

    @ApiOperation("新增或者修改课程")
    @PostMapping("update")
    public CommonResult<Integer> update(@RequestBody Course course, @ApiIgnore Authentication authentication,
                                              @ApiIgnore BindingResult bindingResult){
        if(course.getId() == null)
            return CommonResult.badArgument("id不能为空");
        courseService.saveOrUpdate(course);
        return CommonResult.success(course.getId());
    }


    @ApiImplicitParams({
            @ApiImplicitParam(example = "少儿", value = "模糊匹配", name = "courseName"),
            @ApiImplicitParam(example = "1", value = "页数", name = "pageNum"),
            @ApiImplicitParam(example = "10", value = "页面大小", name = "pageSize"),
            @ApiImplicitParam(example = "true", value = "可见性", name = "visible"),
            @ApiImplicitParam(example = "1", value = "排序字段，1为最热（默认），2为最新", name = "order")
    })
    @ApiOperation("老师查看课程")
    @GetMapping("pageByTeacher")
    public CommonResult page(@RequestParam(required = false) Integer teacherId,
                            @RequestParam(required = false) String courseName,
                             @RequestParam(required = false, defaultValue = "1") int order,
                             @RequestParam(required = false, defaultValue = "1") Boolean visible,
                                     @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                     @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<Course> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        if(!StringUtils.isBlank(courseName))
            wrapper.like("course_name", courseName + "%");
        if(teacherId != null)
            wrapper.eq("course_teacher_id", teacherId);
        if(visible != null)
            wrapper.eq("visible", visible);
        if(order == 2)
            wrapper.orderByDesc("update_time");
        else
            wrapper.orderByDesc("join_count");
        return CommonResult.success(courseService.page(page,wrapper));
    }


    @ApiImplicitParams({
            @ApiImplicitParam(example = "少儿", value = "courseName", name = "模糊匹配"),
            @ApiImplicitParam(example = "20", value = "页面大小", name = "pageSize"),
            @ApiImplicitParam(example = "1", value = "页数", name = "pageNum"),
            @ApiImplicitParam(example = "1", value = "排序字段，1为最热（默认），2为最新", name = "order")
    })
    @ApiOperation("学生查看课程")
    @GetMapping("pageByStudent")
    public CommonResult pageByStudent(
                            @RequestParam(required = false) Integer teacherId,
                            @RequestParam(required = false) String courseName,
                             @RequestParam(required = false, defaultValue = "1") int order,
                             @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                             @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<Course> page = new Page<>(pageNum, pageSize);
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        if(!StringUtils.isBlank(courseName))
            wrapper.like("course_name", courseName + "%");
        wrapper.eq("visible", true);
        if(teacherId != null)
            wrapper.eq("course_teacher_id", teacherId);
        if(order == 2)
            wrapper.orderByDesc("update_time");
        else
            wrapper.orderByDesc("join_count");
        return CommonResult.success(courseMapper.selectPage(page,wrapper));
    }

    @ApiOperation("删除课程")
    @DeleteMapping("{id}")
    public CommonResult<Integer> delete(@PathVariable Integer id){
        courseService.removeById(id);
        return CommonResult.success();
    }

    @ApiOperation("查看自己教的课程")
    @GetMapping("teachCourse")
    public CommonResult teachCourse(@ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        return CommonResult.success(courseService.getByTeacherId(user.getId(),null));
    }

    @ApiOperation("查看某老师教的课程")
    @GetMapping("teacher/{userId}")
    public CommonResult teacherCourse(@PathVariable Integer userId){
        return CommonResult.success(courseService.getByTeacherId(userId, null));
    }

    @ApiOperation("查看某老师教的课程(学生查看)")
    @GetMapping("teacherByStudent/{userId}")
    public CommonResult teacherByStudent(@PathVariable Integer userId){
        return CommonResult.success(courseService.getByTeacherId(userId, true));
    }


}
