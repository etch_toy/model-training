package org.fzs.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.Reference;
import org.fzs.common.entity.Course;
import org.fzs.common.entity.CourseJoin;
import org.fzs.common.entity.User;
import org.fzs.common.service.ICourseJoinService;
import org.fzs.common.service.ICourseService;
import org.fzs.common.service.IUserService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.ExcelUtils;
import org.fzs.common.utils.UserUtils;
import org.fzs.common.vo.AuditVo;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/course")
public class CourseJoinController {
    private final ICourseJoinService iCourseJoinService;
    private final ICourseService courseService;
    @Reference
    private IUserService userService;

    @ApiOperation("本课程的参加学生")
    @GetMapping("{id}/users")
    public CommonResult users(@PathVariable Integer id){
        List<CourseJoin> courseJoinList = iCourseJoinService.getPassByCourseId(id);
        List<Integer> userIds = courseJoinList.stream().map(CourseJoin::getUserId).distinct().collect(Collectors.toList());
        if(userIds.isEmpty()) return CommonResult.success(new ArrayList<>());
        return CommonResult.success(userService.listByIds(userIds));
    }

    /**
     * 导出数据
     *
     * @param response
     * @throws IOException
     */
    @ApiOperation("导出课程参加学生")
    @GetMapping("{id}/exportStudents")
    public void exportExcel(@PathVariable Integer id,  HttpServletResponse response) throws IOException {
        Course course = courseService.getById(id);
        List<CourseJoin> courseJoinList = iCourseJoinService.getPassByCourseId(id);
        List<Integer> userIds = courseJoinList.stream()
                .map(CourseJoin::getUserId).distinct().collect(Collectors.toList());
        List<User> students = userService.listByIds(userIds);
        ExcelUtils.exportExcel(students, course.getCourseName() + "学生名单",
                course.getCourseName() + "学生名单", User.class,
                course.getCourseName() +  "学生名单", response);
    }

    @ApiOperation("本课程还没审核的学生名单")
    @GetMapping("{id}/auditList")
    public CommonResult<List<User>> auditList(@PathVariable Integer id){
        List<CourseJoin> courseJoinList = iCourseJoinService.getByCourseId(id);
        List<Integer> userIds = courseJoinList.stream()
                .filter(courseJoin -> courseJoin.getIsPass() == null)
                .map(CourseJoin::getUserId).distinct().collect(Collectors.toList());
        if(userIds.isEmpty()) return CommonResult.success(new ArrayList<>());
        return CommonResult.success(userService.listByIds(userIds));
    }

    @ApiOperation("本课程的审核情况")
    @GetMapping("{id}/allAuditList")
    public CommonResult<List<AuditVo>> allAuditList(@PathVariable Integer id){
        List<CourseJoin> courseJoinList = iCourseJoinService.getByCourseId(id);
        List<Integer> userIds = courseJoinList.stream()
                .map(CourseJoin::getUserId).distinct().collect(Collectors.toList());
        List<User> users = userService.listByIds(userIds);
        if(users.isEmpty()) return CommonResult.success(new ArrayList<>());
        HashMap<Integer, User> idToUser = new HashMap<>();
        users.forEach(item -> idToUser.put(item.getId(), item));
        List<AuditVo> list = courseJoinList.stream().map(courseJoin -> {
            AuditVo auditVo = new AuditVo();
            User user = idToUser.getOrDefault(courseJoin.getUserId(), User.builder().nickname("已注销用户").build());
            BeanUtils.copyProperties(user, auditVo);
            auditVo.setIsPass(courseJoin.getIsPass());
            return auditVo;
        }).collect(Collectors.toList());
        return CommonResult.success(list);
    }


    @ApiOperation("审核学生")
    @PostMapping("audit")
    public CommonResult audit(@RequestBody CourseJoin courseJoin){
        if(courseJoin.getCourseId() == null) return CommonResult.badArgument("课程id不能为空");
        if(courseJoin.getUserId() == null) return CommonResult.badArgument("用户id不能为空");
        if(courseJoin.getIsPass() == null) return CommonResult.badArgument("审核情况不能为空");
        iCourseJoinService.auditCourseJoin(courseJoin);
        return CommonResult.success();
    }

    @ApiOperation("课程报名")
    @GetMapping("{id}/join")
    public CommonResult join(@PathVariable Integer id, @ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        iCourseJoinService.joinCourse(user.getId(),id);
        return CommonResult.success();
    }

    @ApiOperation("退出课程")
    @PostMapping("{id}/exit")
    public CommonResult exit(@PathVariable Integer id, @ApiIgnore Authentication authentication) {
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        iCourseJoinService.exitCourse(user.getId(),id);
        return CommonResult.success();
    }


    @ApiOperation("查看自己参加的课程")
    @GetMapping("myCourse")
    public CommonResult myCourse(@ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        List<CourseJoin> courseJoinList = iCourseJoinService.getPassByUserId(user.getId());
        List<Integer> courseIds = courseJoinList.stream()
                .map(CourseJoin::getCourseId).distinct().collect(Collectors.toList());
        if(courseIds.isEmpty()) return CommonResult.success(new ArrayList<>());
        return CommonResult.success(courseService.listByIds(courseIds));
    }

    @ApiOperation("查看自己是否参加了此课程")
    @GetMapping("{id}/isJoin")
    public CommonResult isJoin(@PathVariable Integer id, @ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        QueryWrapper<CourseJoin> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", id)
                .eq("user_id", user.getId())
                .orderByDesc("create_time");
        List<CourseJoin> list = iCourseJoinService.list(wrapper);
        CourseJoin one = list.isEmpty() ? null : list.get(0);
        int status = 1; // 已通过
        if(one == null) status = 0; // 未加入
        else if(one.getIsPass() == null) status = 2; // 未审核
        else if(one.getIsPass() != null && !one.getIsPass()) status = 3; // 不通过
        return CommonResult.success(status);
    }
}
