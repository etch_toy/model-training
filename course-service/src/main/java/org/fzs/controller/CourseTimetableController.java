package org.fzs.controller;


import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fzs.common.entity.Course;
import org.fzs.common.entity.CourseJoin;
import org.fzs.common.entity.CourseTimetable;
import org.fzs.common.entity.User;
import org.fzs.common.service.ICourseJoinService;
import org.fzs.common.service.ICourseService;
import org.fzs.common.service.ICourseTimetableService;
import org.fzs.common.service.INoticeService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.UserUtils;
import org.fzs.common.vo.CourseTimetableVo;
import org.fzs.common.vo.WeekCourseTimetable;
import org.fzs.common.vo.WeekList;
import org.fzs.service.NoticeTimerService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author linjintao
 * @since 2021-03-10
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/course")
public class CourseTimetableController {
    private final ICourseService courseService;
    private final ICourseJoinService courseJoinService;
    private final ICourseTimetableService courseTimetableService;
    private final INoticeService noticeService;
    private final NoticeTimerService noticeTimerService;

    @ApiOperation("查看我的课表(学生)")
    @GetMapping("studentCourseTable")
    public CommonResult studentCourseTable(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
            @ApiIgnore Authentication authentication){
        int dayValue = date.getDayOfWeek().getValue();
        // 这周周一的日期
        LocalDate startDate = date.minusDays(dayValue - 1);
        // 这周周日的日期
        LocalDate endDate = date.plusDays(7 - dayValue);
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        List<CourseJoin> courseJoinList = courseJoinService.getPassByUserId(user.getId());
        List<Integer> courseIds = courseJoinList.stream().map(CourseJoin::getCourseId).distinct().collect(Collectors.toList());
        QueryWrapper<CourseTimetable> wrapper = new QueryWrapper<>();
        if(CollectionUtils.isEmpty(courseIds)) {
            WeekList weekList = WeekList.emptyData();
            weekList.setStartDate(startDate);
            weekList.setEndDate(endDate);
            return CommonResult.success(weekList);
        }
        wrapper.in("course_id", courseIds);
        // 排除掉查询时间段区间，比右区间之后才开始的课程，以及在左区间前结束的课程
        wrapper.le("start_time", endDate);
        wrapper.ge("end_time", startDate).or().isNull("end_time");
        List<CourseTimetableVo> voList = courseTimetableService.getVoList(wrapper);
        if(CollectionUtils.isEmpty(voList)) {
            WeekList weekList = WeekList.emptyData();
            weekList.setStartDate(startDate);
            weekList.setEndDate(endDate);
            return CommonResult.success(weekList);
        }
        return CommonResult.success(toWeekList(voList, startDate, endDate));
    }

    @ApiOperation("查看我的课表(老师)")
    @GetMapping("teacherCourseTable")
    public CommonResult teacherCourseTable(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
            @ApiIgnore Authentication authentication){
        int dayValue = date.getDayOfWeek().getValue();
        // 这周周一的日期
        LocalDate startDate = date.minusDays(dayValue - 1);
        // 这周周日的日期
        LocalDate endDate = date.plusDays(7 - dayValue);
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        List<Course> courseList = courseService.getByTeacherId(user.getId(), true);
        List<Integer> courseIds = courseList.stream().map(Course::getId).collect(Collectors.toList());
        QueryWrapper<CourseTimetable> wrapper = new QueryWrapper<>();
        if(CollectionUtils.isEmpty(courseIds)) {
            WeekList weekList = WeekList.emptyData();
            weekList.setStartDate(startDate);
            weekList.setEndDate(endDate);
            return CommonResult.success(weekList);
        }
        wrapper.in("course_id", courseIds);
        // 排除掉查询时间段区间，比右区间之后才开始的课程，以及在左区间前结束的课程
        wrapper.le("start_time", endDate);
        wrapper.ge("end_time", startDate).or().isNull("end_time");
        List<CourseTimetableVo> voList = courseTimetableService.getVoList(wrapper);
        if(CollectionUtils.isEmpty(voList)) {
            WeekList weekList = WeekList.emptyData();
            weekList.setStartDate(startDate);
            weekList.setEndDate(endDate);
            return CommonResult.success(weekList);
        }
        return CommonResult.success(toWeekList(voList, startDate, endDate));
    }

    @ApiOperation("新增或者修改课课表")
    @PostMapping("timetable")
    public CommonResult<Integer> timetable(@Validated @RequestBody CourseTimetableVo courseTimetable,
                                              BindingResult bindingResult){
        String dayJson = null;
        courseTimetable.setWeekDays(null);
        courseTimetable.setStartTime(courseTimetable.getStartDate().atTime(0,0));
        if(courseTimetable.getEndDate() != null)
            courseTimetable.setEndTime(courseTimetable.getEndDate().atTime(0,0));
        List<Integer> weekDayList = courseTimetable.getWeekDayList();
        if(!CollectionUtils.isEmpty(weekDayList)){
            List<Integer> list = weekDayList.stream()
                    .filter(day -> day >= 1 && day <= 7).sorted().collect(Collectors.toList());
            dayJson = JSONArray.toJSONString(list);
        }else if(courseTimetable.getId() == null){
            dayJson = "[]";
        }
        courseTimetable.setWeekDays(dayJson);
        courseTimetableService.saveOrUpdate(courseTimetable);
        // 先取消之前的，再加入现在的
        CourseTimetableVo vo = courseTimetableService.getVoById(courseTimetable.getId());
        noticeTimerService.cancelByTimetableId(vo.getId());
        noticeTimerService.addTimetableNotice(vo);
//        noticeService.sendHasClassNotice(courseTimetable.getId());
        return CommonResult.success(courseTimetable.getId());
    }

    @ApiOperation("删除课表")
    @DeleteMapping("timetable/{id}")
    public CommonResult<Integer> delete(@PathVariable Integer id){
        courseTimetableService.removeById(id);
        noticeTimerService.cancelByTimetableId(id);
        return CommonResult.success();
    }

    @ApiOperation("获取课表")
    @GetMapping("timetable/{id}")
    public CommonResult<CourseTimetableVo> get(@PathVariable Integer id){
        return CommonResult.success(courseTimetableService.getVoById(id));
    }

    @ApiOperation("获取某课程下的课表列表")
    @GetMapping("{courseId}/timetables")
    public CommonResult<List<CourseTimetableVo>> list(@PathVariable Integer courseId){
        QueryWrapper<CourseTimetable> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        return CommonResult.success(courseTimetableService.getVoList(wrapper));
    }



    private WeekList toWeekList(List<CourseTimetableVo> voList, LocalDate startDate, LocalDate endDate){
        WeekList weekList = new WeekList();
        weekList.setStartDate(startDate);
        weekList.setEndDate(endDate);
        HashMap<Integer, List<WeekCourseTimetable>> map = new HashMap<>();
        List<Integer> courseIds = voList.stream().map(CourseTimetable::getCourseId).collect(Collectors.toList());
        List<Course> courseList = courseService.listByIds(courseIds);
        HashMap<Integer, Course> courseIdToCourse = new HashMap<>();
        // 构造courseIdToCourse，方便填充数据
        courseList.forEach(item ->{
            courseIdToCourse.put(item.getId(), item);
        });
        // 初始化map
        for(int i = 1; i <= 7; i++) map.put(i, new ArrayList<>());
        // 主要逻辑，构造WeekCourseTimetable
        voList.forEach(vo ->{
            List<Integer> weekDayList = vo.getWeekDayList();
            Course course = courseIdToCourse.getOrDefault(vo.getCourseId(),
                    Course.builder().id(vo.getCourseId()).courseName("已删除课程").build());
            WeekCourseTimetable weekCourseTimetable = WeekCourseTimetable.builder()
                    .classTime(vo.getClassTime())
                    .courseId(course.getId())
                    .courseName(course.getCourseName())
                    .courseTimetableId(vo.getId())
                    .endTime(vo.getEndTime())
                    .startTime(vo.getStartTime())
                    .build();
            weekDayList.forEach(day ->{
                List<WeekCourseTimetable> dayList = map.get(day);
                dayList.add(weekCourseTimetable);
            });
        });
        // map转
        map.forEach((key, value) -> {
            // 按时间排序
            value.sort(Comparator.comparing(WeekCourseTimetable::getClassTime));
            LocalDateTime endTime = endDate.atTime(0, 0);
            LocalDateTime startTime = startDate.atTime(0, 0);
            // 过滤数据
            List<WeekCourseTimetable> list = value.stream().filter(item -> {
                // 1. 没设置结束时间，查出来
                if (item.getEndTime() == null) return true;
                // 2. 课程结束时间大于查询范围，查出来
                else if(item.getEndTime().isAfter(endTime)) return true;
                // 3. 在区间中，当天的日期在查询的区间才查出来
                LocalDateTime theDay = startTime.plusDays(key - 1);
                return !item.getEndTime().isBefore(theDay) && !item.getStartTime().isAfter(theDay);
            }).collect(Collectors.toList());

            switch (key){
                case 1: weekList.setMonday(list); break;
                case 2: weekList.setTuesday(list); break;
                case 3: weekList.setWednesday(list); break;
                case 4: weekList.setThursday(list); break;
                case 5: weekList.setFriday(list); break;
                case 6: weekList.setSaturday(list); break;
                case 7: weekList.setSunday(list); break;
            }
        });
        return weekList;
    }

}
