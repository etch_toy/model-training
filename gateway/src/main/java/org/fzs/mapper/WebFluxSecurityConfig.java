//package org.org.fzs.config;
//
//import lombok.RequiredArgsConstructor;
//import org.org.fzs.filter.AuthorizationManager;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.convert.converter.Converter;
//import org.springframework.security.authentication.AbstractAuthenticationToken;
//import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
//import org.springframework.security.config.web.server.ServerHttpSecurity;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.oauth2.jwt.Jwt;
//import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder;
//import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
//import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
//import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
//import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
//import org.springframework.security.web.server.SecurityWebFilterChain;
//import org.springframework.security.web.server.csrf.CookieServerCsrfTokenRepository;
//import reactor.core.publisher.Mono;
//
//import javax.crypto.SecretKey;
//import javax.crypto.spec.SecretKeySpec;
//
//import java.util.Collection;
//import java.util.Collections;
//import java.util.stream.Collectors;
//
//import static org.springframework.security.config.Customizer.withDefaults;
//
//@Configuration
//@EnableWebFluxSecurity
//@RequiredArgsConstructor
//public class WebFluxSecurityConfig {
//    private String SIGNING_KEY = "uaa123";
//
////    @Autowired
////    JwtAccessTokenConverter accessTokenConverter;
//    final private AuthorizationManager authorizationManager;
//
//
//    @Bean
//    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
//
//        // 配置jwt认证校验
//    http.oauth2ResourceServer()
////            .jwt(jwt -> jwt.jwkSetUri("http://localhost:8004/uaa/getPublicKey"));
//            .jwt().jwtAuthenticationConverter(jwtAuthenticationConverter());
////            .jwt().jwtAuthenticationConverter(grantedAuthoritiesExtractor());
//        ;
//
////    http.authorizeExchange().pathMatchers(ArrayUtil.toArray(ignoreUrlsConfig.getUrls(),String.class)).permitAll()//白名单配置
////    .anyExchange().access(authorizationManager)//鉴权管理器配置
////        .and().exceptionHandling()
////        .accessDeniedHandler(restfulAccessDeniedHandler)//处理未授权
////        .authenticationEntryPoint(restAuthenticationEntryPoint)//处理未认证
////        .and().csrf().disable();
////            return http.build();
//                http.authorizeExchange(exchanges -> exchanges
//                        .pathMatchers("/uaa/**").permitAll()
////                        .pathMatchers("/user/**").hasAuthority("ROLE_API")
//                        .anyExchange().access(authorizationManager)
////                        .anyExchange().authenticated()
//                )
//                        .csrf(csrf -> csrf.disable());
////                .oauth2ResourceServer(oauth2 -> oauth2
////                        .jwt(withDefaults())
////
////                )
//
////                // ...
//////                .csrf(csrf -> csrf.csrfTokenRepository(CookieServerCsrfTokenRepository.withHttpOnlyFalse()));
//        return http.build();
//    }
//
//
//
//    /**
//     * @linkhttps://blog.csdn.net/qq_24230139/article/details/105091273
//     * ServerHttpSecurity没有将jwt中authorities的负载部分当做Authentication
//     * 需要把jwt的Claim中的authorities加入
//     * 方案：重新定义ReactiveAuthenticationManager权限管理器，默认转换器JwtGrantedAuthoritiesConverter
//     */
//    @Bean
//    public Converter<Jwt, Mono<AbstractAuthenticationToken>> jwtAuthenticationConverter() {
//        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
//        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
//        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("authorities");
//
//        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
//        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
//        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
//    }
////    Converter<Jwt, Mono<AbstractAuthenticationToken>> grantedAuthoritiesExtractor() {
////        JwtAuthenticationConverter jwtAuthenticationConverter =
////                new JwtAuthenticationConverter();
////        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter
////                (new GrantedAuthoritiesExtractor());
////        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
////    }
////
////    static class GrantedAuthoritiesExtractor
////            implements Converter<Jwt, Collection<GrantedAuthority>> {
////
////        public Collection<GrantedAuthority> convert(Jwt jwt) {
////            Collection<?> authorities = (Collection<?>)
////                    jwt.getClaims().getOrDefault("authorities", Collections.emptyList());
////
////            return authorities.stream()
////                    .map(Object::toString)
////                    .map(SimpleGrantedAuthority::new)
////                    .collect(Collectors.toList());
////        }
////    }
//
////    @Bean
////    public ReactiveJwtDecoder jwtDecoder() {
////        new
////        SecretKey secretKey = new SecretKeySpec(SIGNING_KEY);
////        return NimbusReactiveJwtDecoder.withSecretKey().build();
////    }
//}
