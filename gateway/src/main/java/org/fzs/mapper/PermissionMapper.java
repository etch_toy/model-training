package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.fzs.common.dto.PermissionRedisDTO;
import org.fzs.common.entity.Permission;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.Map;

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

    @MapKey("url")
    @Select("select group_concat(role_id) as roleIdsStr, url from t_permission p, t_role_permission rp " +
            "where p.id = rp.permission_id group by url order by null")
    Map<String, PermissionRedisDTO> findRolesGroupUrl();
}
