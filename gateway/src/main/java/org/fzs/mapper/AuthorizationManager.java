//package org.org.fzs.filter;
//
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.security.authorization.AuthorizationDecision;
//import org.springframework.security.authorization.ReactiveAuthorizationManager;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.web.server.authorization.AuthorizationContext;
//import org.springframework.stereotype.Component;
//import org.springframework.util.AntPathMatcher;
//import org.springframework.util.PathMatcher;
//import reactor.core.publisher.Mono;
//import org.org.fzs.common.utils.Constants;
//
//import java.security.Principal;
//import java.util.*;
//
///**
// * 鉴权管理器
// */
//@Component
//@RequiredArgsConstructor
//@Slf4j
//public class AuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {
//    private final RedisTemplate redisTemplate;
//
//    @Override
//    public Mono<AuthorizationDecision> check(Mono<Authentication> mono, AuthorizationContext authorizationContext) {
//        ServerHttpRequest request = authorizationContext.getExchange().getRequest();
//        String path = request.getURI().getPath();
//        PathMatcher pathMatcher = new AntPathMatcher();
//
//
//
//        // 1. 对应跨域的预检请求直接放行
//        if (request.getMethod() == HttpMethod.OPTIONS) {
//            return Mono.just(new AuthorizationDecision(true));
//        }
//
//        // 2. token为空拒绝访问
//        String token = request.getHeaders().getFirst("Authorization");
//        if (StringUtils.isBlank(token)) {
//            return Mono.just(new AuthorizationDecision(false));
//        }
//
//        // 3.缓存取资源权限角色关系列表
//        Map<Object, Object> resourceRolesMap = redisTemplate.opsForHash().entries(Constants.RESOURCE_ROLES_KEY);
//        Iterator<Object> iterator = resourceRolesMap.keySet().iterator();
//
//        // 4.请求路径匹配到的资源需要的角色权限集合authorities
//        List<String> authorities = new ArrayList<>();
//        while (iterator.hasNext()) {
//            String pattern = (String) iterator.next();
//            if (pathMatcher.match(pattern, path)) {
//                String roleIdsStr = resourceRolesMap.get(pattern).toString();
//                String[] split = roleIdsStr.split(",");
//                authorities.addAll(Arrays.asList(split));
//            }
//        }
//
//        Mono<Principal> principal = authorizationContext.getExchange().getPrincipal();
//        Object[] objects = mono
//                .filter(Authentication::isAuthenticated)
//                .flatMapIterable(Authentication::getAuthorities)
//                .toStream().toArray();
//        Mono<AuthorizationDecision> authorizationDecisionMono =  mono
//                .filter(authentication -> {
//                    System.out.println("+++++");
//                    return authentication.isAuthenticated();
//                })
//                .flatMapIterable(Authentication::getAuthorities)
//                .map(grantedAuthority -> {
//                    System.out.println("-----");
//                    return grantedAuthority.getAuthority();
//                })
//                .any(roleId -> {
//                    // 5. roleId是请求用户的角色(格式:ROLE_{roleId})，authorities是请求资源所需要角色的集合
//                    log.info("访问路径：{}", path);
//                    log.info("用户角色roleId：{}", roleId);
//                    log.info("资源需要权限authorities：{}", authorities);
//                    return authorities.contains(roleId);
//                })
//                .map(AuthorizationDecision::new)
//                .defaultIfEmpty(new AuthorizationDecision(false));
////        return authorizationDecisionMono;
//        return Mono.just(new AuthorizationDecision(true));
//
//    }
//
//}
