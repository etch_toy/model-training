//package org.org.fzs.filter;
//
//import com.alibaba.fastjson.JSON;
//import com.nimbusds.jose.JWSObject;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.context.ReactiveSecurityContextHolder;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.oauth2.jwt.Jwt;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import org.springframework.security.oauth2.provider.OAuth2Request;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//import org.org.fzs.common.utils.EncryptUtil;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * 全局的登录鉴权过滤器，所有gateway共用
// */
//@Component
//@Slf4j
//public class LoginFilter implements GlobalFilter, Ordered{
//
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        String token = exchange.getRequest().getHeaders().getFirst("Authorization");
//        if (StringUtils.isEmpty(token)) {
//            return chain.filter(exchange);
//        }
//        try {
//            //从token中解析用户信息并设置到Header中去
//            String realToken = token.replace("Bearer ", "");
////            Jwt jwt = Jwt.withTokenValue(realToken).build();
//            JWSObject jwsObject = JWSObject.parse(realToken);
//
//            String userStr = jwsObject.getPayload().toString();
//            log.info("AuthGlobalFilter.filter() user:{}",userStr);
//            ServerHttpRequest request = exchange.getRequest().mutate().header("user", userStr).build();
//            exchange = exchange.mutate().request(request).build();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        //从安全上下文中拿 到用户身份对象
////        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
////        if (!(authentication instanceof OAuth2Authentication)) {
//////            //如果不存在 : 认证失败
//////            exchange.getResponse().setStatusCo   de(HttpStatus.UNAUTHORIZED);
//////            return exchange.getResponse().setComplete(); //请求结束
////            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
////                log.info(" 后置 : " + exchange.getResponse().getStatusCode() +
////                        " " + exchange.getRequest().getURI().toString());
////            }));
////        }
////        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
////        Authentication userAuthentication = oAuth2Authentication.getUserAuthentication();
////        if (userAuthentication == null) {
////            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
////            return exchange.getResponse().setComplete(); //请求结束
////        }
////        //取出用户身份信息
////        String principal = userAuthentication.getName();
////        //从userAuthentication取出权限，放在authorities
////        List<String> authorities = userAuthentication.getAuthorities()
////                .stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
////        OAuth2Request oAuth2Request = oAuth2Authentication.getOAuth2Request();
////        Map<String, String> requestParameters = oAuth2Request.getRequestParameters();
////        Map<String, Object> jsonToken = new HashMap<>(requestParameters);
////        jsonToken.put("principal", principal);
////        jsonToken.put("authorities", authorities);
////
////        exchange.getRequest().getHeaders()
////                .add("json-token", EncryptUtil.encodeUTF8StringBase64(JSON.toJSONString(jsonToken)));
//        //4.如果存在,继续执行
//        return chain.filter(exchange); //继续向下执行
//    }
//
//
//    /**
//     * 指定过滤器的执行顺序 , 返回值越小,执行优先级越高
//     */
//    @Override
//    public int getOrder() {
//        return -1;
//    }
//}