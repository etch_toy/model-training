package org.fzs.authorization;


import lombok.extern.slf4j.Slf4j;
import org.fzs.common.utils.Constants;
import org.fzs.run.PermissionLoad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import reactor.core.publisher.Mono;

import java.util.*;

/**
 * 鉴权管理器，用于判断是否有资源的访问权限
 * Created by linjintao on 2021/3/1.
 */
@Component
@Slf4j
public class AuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private PermissionLoad load;

    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> mono, AuthorizationContext authorizationContext) {
        //从Redis中获取当前路径可访问角色列表
        ServerHttpRequest request = authorizationContext.getExchange().getRequest();
        String path = request.getURI().getPath();
        log.info("访问" + path);
        if(request.getMethod() != null)
            log.info(request.getMethod().toString());
        log.info(request.getHeaders().toString());
        if(request.getMethod().equals(HttpMethod.OPTIONS)){
            return Mono.just(new AuthorizationDecision(true));
        }
        PathMatcher pathMatcher = new AntPathMatcher();
//        Object obj = redisTemplate.opsForHash().get(Constants.RESOURCE_ROLES_KEY, uri.getPath());
        //        // 3.缓存取资源权限角色关系列表
        if(!redisTemplate.hasKey(Constants.RESOURCE_ROLES_KEY)){
            load.load();
        };
        Map<Object, Object> resourceRolesMap = redisTemplate.opsForHash().entries(Constants.RESOURCE_ROLES_KEY);
        Iterator<Object> iterator = resourceRolesMap.keySet().iterator();

        // 4.请求路径匹配到的资源需要的角色权限集合authorities
        List<String> authorities = new ArrayList<>();
        while (iterator.hasNext()) {
            String pattern = (String) iterator.next();
            if (pathMatcher.match(pattern, path)) {
                String roleIdsStr = resourceRolesMap.get(pattern).toString();
                String[] split = roleIdsStr.split(",");
                authorities.addAll(Arrays.asList(split));
            }
        }
        // 此路径需要的权限集合
//        authorities = authorities.stream().map(i -> i = AuthConstant.AUTHORITY_PREFIX + i).collect(Collectors.toList());
        //认证通过且角色匹配的用户可访问当前路径
        Mono<AuthorizationDecision> result = mono
                .filter(Authentication::isAuthenticated)
//                .switchIfEmpty(Mono.just(new TourGrantedAuthority(null)))
                .flatMapIterable(Authentication::getAuthorities)
                .flatMap(grantedAuthority -> Mono.just(Optional.of(grantedAuthority)))
                // 为空时的处理
                .defaultIfEmpty(Optional.empty())
                .map(optional ->
                        // 默认游客的权限
                     optional.isEmpty() ? "ROLE_4" : optional.get().getAuthority()
                )
                .any(roleIdStr ->{
                    String roleId = roleIdStr.replaceFirst(Constants.AUTHORITY_PREFIX, "");
                    return authorities.contains(roleId);
                })
                .map(AuthorizationDecision::new)
                .defaultIfEmpty(new AuthorizationDecision(false));
        return result;
//        return Mono.just(new AuthorizationDecision(true));
//                        authorities.contains("4") ? new AuthorizationDecision(true)
//                        : new AuthorizationDecision(false));
//        return Mono.just(new AuthorizationDecision(true));
    }


    /**
     * 游客鉴权
     */
    public static class TourAuthentication extends AbstractAuthenticationToken {

        public TourAuthentication(Collection<? extends GrantedAuthority> authorities) {
            super(authorities);
        }

        @Override
        public Object getCredentials() {
            return null;
        }

        @Override
        public Object getPrincipal() {
            return "ROLE_4";
        }

    }

    public static class TourGrantedAuthority implements GrantedAuthority{

        @Override
        public String getAuthority() {
            return "ROLE_4";
        }
    }
    public static void main(String[] args) {
        PathMatcher pathMatcher = new AntPathMatcher();
        String pa = "/course/{id:[0-9]+}";
        String path = "/course/1";
        String path1 = "/course/11";
        String path2 = "/course/aa";
        boolean match = pathMatcher.match(pa, path);
        boolean match1 = pathMatcher.match(pa, path1);
        boolean match2 = pathMatcher.match(pa, path2);
        System.out.println("match = " + match);
        System.out.println("match = " + match1);
        System.out.println("match = " + match2);
//        输出：
//        match = true
//        match = true
//        match = false
    }
}
