package org.fzs.run;

import lombok.RequiredArgsConstructor;
import org.fzs.common.dto.PermissionRedisDTO;
import org.fzs.common.utils.Constants;
import org.fzs.mapper.PermissionMapper;
import org.springframework.boot.ApplicationArguments;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class PermissionLoad {
    private final PermissionMapper permissionMapper;
    private final RedisTemplate<String, Object> redisTemplate;

    /**
     * 启动时把部分数据从数据库读出存到redis
     */
    public void load(){
        Map<String, PermissionRedisDTO> urlToPerRole = permissionMapper.findRolesGroupUrl();
        Map<String, String> redisMap = new HashMap<>();
        urlToPerRole.forEach((k,v) ->{
            redisMap.put(k, v.getRoleIdsStr());
        });
        redisTemplate.opsForHash().putAll(Constants.RESOURCE_ROLES_KEY, redisMap);
        redisTemplate.expire(Constants.RESOURCE_ROLES_KEY, 1024, TimeUnit.DAYS);
    }
}
