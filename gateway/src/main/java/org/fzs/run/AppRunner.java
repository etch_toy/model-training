package org.fzs.run;

import org.fzs.common.dto.PermissionRedisDTO;
import lombok.RequiredArgsConstructor;
import org.fzs.mapper.PermissionMapper;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.fzs.common.utils.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class AppRunner implements ApplicationRunner {
    private final PermissionLoad load;

    /**
     * 启动时把部分数据从数据库读出存到redis
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        load.load();
    }
}
