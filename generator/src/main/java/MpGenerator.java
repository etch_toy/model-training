import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * <p>
 * 代码生成器演示
 * </p>
 */
public class MpGenerator {


    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        String moduleName = "generator";
        gc.setOutputDir(projectPath + "/" + moduleName + "/src/main/java");
        gc.setAuthor("linjintao");
        gc.setOpen(false);
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(true);
        //gc.setControllerName("SSSSScontroller");
        // 是否覆盖已有文件
        gc.setFileOverride(true);
        gc.setSwagger2(true);
//        gc.setIdType(IdType.AUTO);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://121.4.149.22:3306/model_training?characterEncoding=utf8&" +
                "useSSL=false&serverTimezone=Asia/Shanghai&useLegacyDatetimeCode=false&allowPublicKeyRetrieval=true");
        dsc.setUsername("root");
        dsc.setPassword("root");
//        dsc.setUrl("jdbc:mysql://159.226.20.103:7005/education?characterEncoding=utf8&useSSL=false&" +
//                "serverTimezone=Asia/Shanghai&useLegacyDatetimeCode=false&allowPublicKeyRetrieval=true");
//        dsc.setUsername("education");
//        dsc.setPassword("SQL_EDUCATION_6");
//        dsc.setUrl("jdbc:mysql://localhost:3306/education?characterEncoding=utf8&useSSL=false&" +
//                "serverTimezone=Asia/Shanghai&useLegacyDatetimeCode=false&allowPublicKeyRetrieval=true");
        // dsc.setSchemaName("public");

//        dsc.setUsername("root");
//        dsc.setPassword("MySQL/80/root:pwd");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        mpg.setDataSource(dsc);

        /** 包配置 **/
        PackageConfig pc = new PackageConfig();
//        pc.setModuleName(moduleName);
        //pc.setModuleName(scanner("模块名"));
        pc.setParent(null);
        // 这个地址是生成的配置文件的包路径
//        pc.setEntity("com.cikers.ps.model.org.org.fzs.common.org.fzs.entity");

        //pc.setController("com.cikers.ps.org.fzs.controller");
//        pc.setMapper("com.cikers.ps.org.fzs.mapper");
        pc.setController("org/fzs/controller");
        pc.setEntity("org/fzs/entity");
        pc.setService("org/fzs/service");
        pc.setMapper("org/fzs/mapper");
        mpg.setPackageInfo(pc);
        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
//        String templatePath = "/templates/org.fzs.mapper.xml.vm";

//        // 自定义输出配置
//        List<FileOutConfig> focList = new ArrayList<>();
//        // 自定义配置会被优先输出
//        focList.add(new FileOutConfig(templatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名
//                return projectPath + "/" + moduleName + "/src/main/resources/org.fzs.mapper"
//                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
//            }
//        });
//
//        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

//		 //配置自定义输出模板
        // 不需要其他的类型时，直接设置为null就不会成对应的模版了
        //templateConfig.setEntity("...");
//        templateConfig.setService("/templates/org.fzs.service.java");
//        templateConfig.setController(null);
//        templateConfig.setServiceImpl("/templates/serviceImpl.java");
// 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也
        // 可以自定义模板名称 只要放到目录下，名字不变 就会采用这个模版 下面这句有没有无所谓
        // 模版去github上看地址：
        /**https://github.com/baomidou/mybatis-plus/tree/3.0/mybatis-plus-generator/src/main/resources/templates*/
        //templateConfig.setEntity("/templates/org.org.fzs.common.org.fzs.entity.java");
//        templateConfig.setXml(null);
        // 配置模板
//        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 数据库映射实体的命名
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 数据表字段映射
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
//        strategy.setSuperEntityClass("com.cikers.ps.model.BaseEntity");
        // 设置继承的mapper
//        strategy.setSuperMapperClass("tk.mybatis.MyMapper");
        strategy.setEntityLombokModel(true);
        // 下面两个设置为null无效，我吐血，看了下模板，没法改，只能自定义模板
//        strategy.setSuperServiceImplClass(null);
//        strategy.setSuperServiceClass(null);
        // 生成@RestController
        strategy.setRestControllerStyle(true);
        //strategy.setRestControllerStyle(false);
        //strategy.setSuperControllerClass("com.cikers.ps.org.fzs.controller.MysqlController");
        // 要生成的表名
        strategy.setInclude("activity_apply");
        // 设置继承的父类字段
//        strategy.setSuperEntityColumns("id","modifiedBy","modifiedOn","createdBy","createdOn");
        //strategy.setControllerMappingHyphenStyle(true);
        //strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }


}