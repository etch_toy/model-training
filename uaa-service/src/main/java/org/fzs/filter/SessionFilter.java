//package org.fzs.filter;
//
//import org.apache.catalina.connector.Response;
//import org.apache.catalina.connector.ResponseFacade;
//import org.apache.tomcat.util.http.MimeHeaders;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//import org.fzs.common.utils.SessionVerifyCodeUtils;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.IOException;
//import java.lang.reflect.Field;
//
//@Component
//@Order(0)
//@WebFilter(filterName = "sessionFilter", urlPatterns = "/*")
//public class SessionFilter extends OncePerRequestFilter {
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
////        HttpSession session = request.getSession(false);
////        String sessionId = "";
////        if(session == null) {
////            Cookie[] cookies = request.getCookies();
////            if(cookies != null) {
////                for (Cookie cookie : cookies) {
////                    if (cookie.getName().equals("JSESSIONID")) {
////                        sessionId = cookie.getValue();
////                        break;
////                    }
////                }
////            }
////        }
////        if(sessionId.isBlank()){
////            session = request.getSession();
////            sessionId = session.getId();
////        }
////        SessionVerifyCodeUtils.setSessionId(sessionId);
////
////        // 若session跟传来的id相同，不设置set-cookie
////        if(request.getRequestURI().equals("/oauth/token")){
////            Cookie cookie = new Cookie("JSESSIONID", sessionId);
////            response.addCookie(cookie);
////            HttpSession newSession = request.getSession();
////            if(sessionId.equals(newSession.getId())) {
////                filterChain.doFilter(request,response);
////                return;
////            }
////            Field declaredField = null;
////            try {
////
////                declaredField = ResponseFacade.class.getDeclaredField("response");
////                declaredField.setAccessible(true);
////                HttpServletResponseWrapper resp = (HttpServletResponseWrapper) declaredField.get(response);
////                org.apache.coyote.Response coyoteResponse = resp.getCoyoteResponse();
////                MimeHeaders mimeHeaders = coyoteResponse.getMimeHeaders();
////                mimeHeaders.removeHeader("Set-Cookie");
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////        }
//
//        filterChain.doFilter(request,response);
//
//    }
//}
