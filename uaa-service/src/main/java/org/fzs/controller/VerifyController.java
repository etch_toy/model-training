package org.fzs.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.fzs.common.utils.CommonResult;
//import org.fzs.common.utils.SessionVerifyCodeUtils;
import org.fzs.common.utils.VerifyUtil;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Api(tags = "验证码服务")
@RestController
@RequestMapping("/verify")
@RequiredArgsConstructor
public class VerifyController {

    private final RedisTemplate<String, String> redisTemplate;
    public static final String KEY_PREFIX = "SESSION_VERIFY_CODE_";
    public static final String REDIS_KEY_PREFIX = "VERIFY_CODE_";
    private final PasswordEncoder passwordEncoder;

    @ApiOperation("请求生成随机码")
    @GetMapping("/getRandomCode")
    @ApiImplicitParams({
    })
    public CommonResult getRandomCode() throws IOException {
        String preOriginCode = UUID.randomUUID().toString().replace("-", "").substring(0, 8);
        String originCode = preOriginCode + LocalDateTime.now().plusMinutes(5).toEpochSecond(ZoneOffset.of("+8"));
        String imageCode = originCode + "-" + passwordEncoder.encode(originCode);
        return CommonResult.success(imageCode);
    }

    /**
     * 生成验证码的接口
     *
     * @param request  Request对象
     */
    @ApiOperation("请求生成图片验证码图片")
    @GetMapping("/getImageCode")
    @ApiImplicitParams({
    })
    public void getImageCode(@RequestParam String randomCode, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] split = randomCode.split("-");
        if(split.length != 2) {
            this.responseFail(response, CommonResult.badArgument("请传入正确的randomCode"));
            return;
        }
        String originCode = split[0];
        String signature = split[1];
        if(!passwordEncoder.matches(originCode, signature)) {
            this.responseFail(response, CommonResult.badArgument("请传入正确的randomCode"));
            return;
        }
        String time = originCode.substring(8);
        Long timeStamp = Long.valueOf(time);
        LocalDateTime end = LocalDateTime.ofEpochSecond(timeStamp, 0, ZoneOffset.of("+8"));
        if(LocalDateTime.now().isAfter(end)) {
            this.responseFail(response, CommonResult.badArgument("随机码已过期"));
            return;
        }

        // 获取到session
//        HttpSession session = request.getSession();
        // 取到sessionId
//        String id = session.getId();

        // 利用图片工具生成图片
        // 返回的数组第一个参数是生成的验证码，第二个参数是生成的图片
        Object[] objs = VerifyUtil.newBuilder()
                .setWidth(200)   //设置图片的宽度
                .setHeight(50)   //设置图片的高度
                .setSize(4)      //设置字符的个数
                .setLines(2)    //设置干扰线的条数
                .setFontSize(40) //设置字体的大小
                .setTilt(true)   //设置是否需要倾斜
                .setBackgroundColor(Color.LIGHT_GRAY) //设置验证码的背景颜色
                .build()         //构建VerifyUtil项目
                .createImage();  //生成图片
        // 将验证码存入Session
//        session.setAttribute(KEY_PREFIX + id, objs[0]);
        // sessionId需要先设置
//        SessionVerifyCodeUtils.setSessionId(id);
//        SessionVerifyCodeUtils.setVerifyCode(objs[0].toString());

        // 打印验证码
        log.info("生成的验证码:"+objs[0]);

        // 设置redis值的序列化方式
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        // 在redis中保存一个验证码最多尝试次数
        redisTemplate.opsForValue().set(REDIS_KEY_PREFIX + randomCode, objs[0].toString(), 5 * 60, TimeUnit.SECONDS);

        // 将图片输出给浏览器
        BufferedImage image = (BufferedImage) objs[1];
        try {
            response.setHeader("Content-Type", "image/jpeg");//去除缓存
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", -1);
//            Cookie cookie = new Cookie("VERIFY", imageCode);
//            cookie.setPath("/");
//            response.addCookie(cookie);
            ImageIO.write(image, "jpg", response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void responseFail(HttpServletResponse response, CommonResult commonResult){
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.setStatus(400);
        try {
            ObjectMapper mapper = new ObjectMapper();
            response.getWriter().write(mapper.writeValueAsString(commonResult));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

