package org.fzs.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.fzs.common.entity.Role;
import org.fzs.common.entity.User;
import org.fzs.common.exception.CustomException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.fzs.controller.VerifyController;
import org.fzs.mapper.PermissionMapper;
import org.fzs.mapper.RoleMapper;
import org.fzs.mapper.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpringDataUserDetailsService implements UserDetailsService {

    private final UserMapper userMapper;
    private final PermissionMapper permissionMapper;
    private final RoleMapper roleMapper;
    private final RedisTemplate<String, String> redisTemplate;

    //根据 账号查询用户信息
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 1. 检验验证码
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        if (!verifyImageCode(request)) {
            throw new CustomException("验证码有误!");
        }
        if (StringUtils.isEmpty(username)) {
            throw new CustomException("请提供用户名！");
        }
        //2. 将来连接数据库根据账号查询用户信息
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        User user = userMapper.selectOne(wrapper);
        if (Objects.isNull(user)) {
            throw new CustomException("用户名或密码错误！");
        }
        //根据用户的id查询用户的权限
//        List<Permission> permissionList = permissionMapper.findByUserId(user.getId());
//        List<String> permissions = permissionList.stream().map(Permission::getCode).collect(Collectors.toList());
        //将permissions转成数组
//        String[] permissionArray = new String[permissions.size()];
//        permissions.toArray(permissionArray);
        // 3. 查询用户有的角色
        List<Role> roleList = roleMapper.findByUserId(user.getId());
        List<String> roleIds = roleList.stream().map(Role::getId).map(Object::toString).collect(Collectors.toList());
        String[] roleIdsArray= new String[roleIds.size()];
        roleIds.toArray(roleIdsArray);
        //4. 将user转成json写到令牌里
//        user.setPassword(null);
        User principalUser = new User();
        BeanUtils.copyProperties(user, principalUser);
        principalUser.setPassword(null);
        String principal = JSONObject.toJSONString(principalUser);
        UserDetails userDetails = org.springframework.security.core.userdetails.User
                .withUsername(principal)
//                .roles()
                .password(user.getPassword())
                .authorities(roleIdsArray)
//                .authorities()
                .build();
        return userDetails;
    }


    private boolean verifyImageCode(HttpServletRequest request) {

//        HttpSession session = request.getSession();
//        Cookie[] cookies = request.getCookies();
//        Cookie[] cookies = request.getCookies();
//        String sessionId = null;
//        if(cookies != null) {
//            for (Cookie cookie : cookies) {
//                if (cookie.getName().equals("VERIFY")) {
//                    sessionId = cookie.getValue();
//                    break;
//                }
//            }
//        }
        String code = request.getParameter("verify");
        String imageCode = request.getParameter("randomCode");
        if(imageCode == null) return false;
//        String id = session.getId();
//        String id = SessionVerifyCodeUtils.getSessionId();

        // 将redis中的尝试次数减一
        String verifyCodeKey = VerifyController.REDIS_KEY_PREFIX + imageCode;
//        long num = redisTemplate.opsForValue().decrement(verifyCodeKey);
//
//        // 如果次数次数小于0 说明验证码已经失效
//        if (num < 0) {
//            return false;
//        }

        // 将session中的取出对应session id生成的验证码
//        String serverCode = (String) session.getAttribute(VerifyController.KEY_PREFIX + id);
//        String serverCode = SessionVerifyCodeUtils.getVerifyCode();
        Object o = redisTemplate.opsForValue().get(verifyCodeKey);
        if(o == null) return false;
        String serverCode = o.toString();
        // 校验验证码
        if (null == serverCode || null == code || !serverCode.toUpperCase().equals(code.toUpperCase())) {
            return false;
        }

        // 验证通过之后手动将验证码失效
        redisTemplate.delete(verifyCodeKey);
        return true;
    }
}