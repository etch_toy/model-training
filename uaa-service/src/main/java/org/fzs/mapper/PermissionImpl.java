//package org.fzs.mapper;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import org.apache.ibatis.annotations.Insert;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.fzs.common.entity.Permission;
//import org.fzs.common.service.IPermissionService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
//import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
//import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
//import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
//
//import javax.annotation.PostConstruct;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.function.Predicate;
//
//@Service
//public class PermissionImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {
//
//    @Autowired
//    PermissionMapper mapper;
//    @Autowired
//    RequestMappingHandlerMapping requestMappingHandlerMapping;
////    @Autowired
////    IPermissionService permissionService;
//
//    @PostConstruct
//    void init() {
//        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
//        Map<String, Predicate<Class<?>>> pathPrefixes = requestMappingHandlerMapping.getPathPrefixes();
//        Set<String> keySet = pathPrefixes.keySet();
//        for (RequestMappingInfo rmi : handlerMethods.keySet()) {
//            PatternsRequestCondition path = rmi.getPatternsCondition();
//            RequestMethodsRequestCondition methodsCondition = rmi.getMethodsCondition();
//            Set<String> patterns = path.getPatterns();
////            Set<RequestMethod> methods = methodsCondition.getMethods();
////            methods.forEach(System.out::print);
////            System.out.print(" ");
//            Set<String> pathSet = new HashSet<>();
//            patterns.forEach(str -> {
//                String replace = str.replaceAll("(?<=\\{)[A-Za-z]*(?=\\})", "id:[0-9]*");
//                pathSet.add(replace);
////                System.out.println(replace);
//            });
////            List<Permission> list = pathSet.stream().map(item -> {
////                Permission permission = new Permission();
////                permission.setUrl(item);
////                return permission;
////            }).collect(Collectors.toList());
//            List<Permission> list = this.list();
//            list.forEach(item -> {
//                try{
//                mapper.insertRolePermission(2, item.getId());
//                }catch (Exception e){
//
//                }
//            });
////            this.saveBatch(list, 100);
////            patterns.forEach(System.out::println);
////            System.out.println(path);
////            if (handlerMethod.hasMethodAnnotation(NoLogin.class)) {
////                PatternsRequestCondition prc = rmi.getPatternsCondition();
////                Set<String> patterns = prc.getPatterns();
////                noLoginUrlSet.addAll(patterns);
////            }
//        }
//    }
//}
//
//
//@Mapper
//interface PermissionMapper extends BaseMapper<Permission> {
//
//    @Insert("insert into t_role_permission values(#{roleId}, #{permissionId})")
//    void insertRolePermission(@Param("roleId") Integer roleId, @Param("permissionId") Integer permissionId);
//}
