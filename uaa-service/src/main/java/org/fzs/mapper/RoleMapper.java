package org.fzs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.fzs.common.entity.Role;

import java.util.List;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    @Select("select r.* from t_role r, t_user_role ur " +
            "where r.id = ur.role_id and r.deleted = 0 and ur.user_id = #{userId} ")
    List<Role> findByUserId(@Param("userId") Integer userId);

    @Insert("insert into t_user_role(user_id, role_id)  value(#{userId}, #{roleId})")
    void insertUserRole(@Param("userId") Integer userId, @Param("roleId") Integer roleId);

    @Delete("delete from t_user_role where user_id = #{userId}")
    void deleteByUserId(@Param("userId") Integer userId);
}
