package org.fzs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.fzs.common.entity.Role;
import org.fzs.common.entity.User;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Select("<script> select u.* from user u, t_role r, t_user_role ur " +
            "where  u.id = ur.user_id " +
            "<if test='user.nickname != null'> and u.nickname like concat('%',#{user.nickname}, '%')</if>" +
            "and ur.role_id = r.id and r.role_name=#{roleName} and u.deleted = 0 </script>")
    IPage<User> selectByRoleName(@Param("roleName") String roleName, @Param("user") User user,
                                 @Param("page") Page page);


    @Select("select r.* from user u, t_role r, t_user_role ur " +
            "where  u.id = ur.user_id and u.id = #{userId} and u.deleted = 0")
    List<Role> selectRolesByUserId(@Param("userId") Integer userId);

}
