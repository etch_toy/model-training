package org.fzs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.fzs.common.entity.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
    @Select("    SELECT" +
            "        p.*" +
            "    FROM" +
            "        user u," +
            "        t_user_role ur," +
            "        t_role_permission rp," +
            "        t_permission p" +
            "    WHERE" +
            "        u.id = ur.user_id" +
            "        AND ur.role_id = rp.role_id" +
            "        AND p.id = rp.permission_id")
    List<Permission> findByUserId(@Param("userId") Integer userId);
}
