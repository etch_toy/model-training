package org.fzs.user.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.fzs.common.utils.CommonResult;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

@Component
@Aspect
public class BindResultAspect {

    @Pointcut("execution(public * org.fzs.user.controller..*.*(..))")
    public void controllerPointCut() {}

    @Pointcut("args(.., org.springframework.validation.BindingResult)")
    public void argsBindResultPointCut() {}

    @Around("controllerPointCut() && args(..,bindingResult))")
    public Object validateParam(ProceedingJoinPoint joinPoint, BindingResult bindingResult) throws Throwable {
        if(bindingResult.hasErrors()){
            List<ObjectError> errors = bindingResult.getAllErrors();
            // 只抛出第一个校验错误
            ObjectError error = errors.get(0);
            return CommonResult.badArgument(error.getDefaultMessage());
        }
        return joinPoint.proceed();
    }

}
