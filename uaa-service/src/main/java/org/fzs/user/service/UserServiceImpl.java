package org.fzs.user.service;


import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mail.util.MailSSLSocketFactory;
import lombok.RequiredArgsConstructor;
import org.fzs.common.dto.UserDTO;
import org.fzs.common.entity.Role;
import org.fzs.common.entity.User;
import org.fzs.common.exception.CustomException;
import org.fzs.common.service.IUserService;
import org.fzs.common.utils.Constants;
import org.fzs.mapper.RoleMapper;
import org.fzs.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Service
@Component
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    private final UserMapper userMapper;
    private final RoleMapper roleMapper;
    private final RedisTemplate<String, String> redisTemplate;
    @Value("email.host")
    private String stmpHost = "smtp.qq.com";
    @Value("email.port")
    private Integer stmpPort = 465;
    @Value("email.username")
    private String username = "735029684@qq.com";
    @Value("email.passowrd")
    private String password = "zqbryyufyfnibefe";


    @Override
    @Transactional
    public Integer addUser(UserDTO userDTO) {
        List<Integer> roleIds = userDTO.getRoleIds();
        userDTO.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        try {
            this.saveOrUpdate(userDTO);
        }catch (DuplicateKeyException e){
            log.debug(e.getMessage());
            throw new CustomException("邮箱或者用户名重复");
        }
        if(!CollectionUtils.isEmpty(roleIds)) {
            roleMapper.deleteByUserId(userDTO.getId());
            roleIds.stream().distinct().forEach(roleId -> roleMapper.insertUserRole(userDTO.getId(), roleId));
        }
        return userDTO.getId();
    }

    @Override
    @Transactional
    public void addUserList(List<UserDTO> userDTO) {
//        userDTO.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDTO.forEach(this::addUser);

    }

    @Override
    public IPage<User> getTeachers(Page page, User user) {

        return userMapper.selectByRoleName("teacher",user,  page);
    }

    @Override
    public IPage<User> getStudents(Page page, User user) {
        return userMapper.selectByRoleName("student", user, page);
    }

    @Override
    public IPage<User> getAdmins(Page page, User user) {
        return userMapper.selectByRoleName("admin", user, page);
    }

    @Override
    public IPage<User> getByRoleName(Page page, String roleName) {
        return userMapper.selectByRoleName(roleName, new User(), page);
    }

    @Override
    public void sendVerifyEmail(String email) {
        // 生成6位数的验证码，有效时间为2个小时
        Integer code = (int) ((Math.random() * 9 + 1) * 100000);
        String key = Constants.EMAIL_CODE_PRE + email;
        redisTemplate.opsForValue().set(key, code.toString(), 2, TimeUnit.HOURS);
        // 发送邮件进行验证， 字符串相加编译器会自动优化
        String title = "【" + "少儿模特培训机构管理与推广平台"+ "】 欢迎您";
        String content = "亲爱的用户， 您的验证码是【" + code +
                "】<br> 此验证码有效期为 2 小时。 请尽快使用<br>如果不是您本人操作，请无视此邮件";
        try {
            sendMessage(title, content, email);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyEmailCode(String email, Integer code) throws CustomException {
        String key = Constants.EMAIL_CODE_PRE + email;
        String s = redisTemplate.opsForValue().get(key);
        if (s == null)
            throw new CustomException("验证码已失效，请重新获取");
        Integer cacheCode = Integer.valueOf(s);
        if (!cacheCode.equals(code))
            throw new CustomException("验证码错误");
        redisTemplate.delete(key);
    }

    @Override
    public List<Role> getRoles(User user) {
        return userMapper.selectRolesByUserId(user.getId());
    }


    private void sendMessage(String title,  String content, String targetMail) throws Exception {

        // 创建一个配置文件并保存
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol","smtp");
        properties.setProperty("mail.smtp.auth","true");
        properties.setProperty("mail.host",stmpHost);
        properties.setProperty("mail.stmp.port", stmpPort.toString());

        // 设置SSL加密
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.ssl.socketFactory", sf);


        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
        //创建一个session对象, 设置邮箱地址跟授权码
        Session session;
        try {
            session = Session.getDefaultInstance(properties, authenticator);
        }catch (SecurityException e){
            session = Session.getInstance(properties, authenticator);
        }

        //获取连接对象
        Transport transport = session.getTransport();
        //连接服务器
        transport.connect("smtp.qq.com", stmpPort, username, password);
        //创建邮件对象
        MimeMessage mimeMessage = new MimeMessage(session);
        //邮件发送人
        mimeMessage.setFrom(new InternetAddress(username));
        //邮件接收人
        mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(targetMail));
        //邮件标题
        mimeMessage.setSubject(title);
        //邮件内容
        mimeMessage.setContent(content, "text/html;charset=UTF-8");
        //发送邮件
        transport.sendMessage(mimeMessage,mimeMessage.getAllRecipients());
        //关闭连接
        transport.close();
    }
}
