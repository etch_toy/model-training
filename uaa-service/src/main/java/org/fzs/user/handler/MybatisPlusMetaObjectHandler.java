package org.fzs.user.handler;

import org.fzs.common.handler.BaseMybatisPlusMetaObjectHandler;
import org.springframework.stereotype.Component;

/**
 * mybatis-plus字段处理
 */
@Component
public class MybatisPlusMetaObjectHandler extends BaseMybatisPlusMetaObjectHandler {
}
