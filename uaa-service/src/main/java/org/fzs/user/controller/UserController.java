package org.fzs.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.fzs.common.dto.ResetPassword;
import org.fzs.common.dto.UserDTO;
import org.fzs.common.entity.Role;
import org.fzs.common.entity.User;
import org.fzs.common.exception.CustomException;
import org.fzs.common.service.IUserService;
import org.fzs.common.utils.CommonResult;
import org.fzs.common.utils.ExcelUtils;
import org.fzs.common.utils.UserUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final IUserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
//    @Reference
//    private ICourseTimetableService iCourseTimetableService;
//    @Reference
//    private ICourseJoinService courseJoinService;

    @ApiOperation("查看用户个人信息")
    @GetMapping("{id}")
    public CommonResult<User> profile(@PathVariable Integer id){
        User user = userService.getById(id);
        user.setPassword(null);
        return CommonResult.success(user);
    }

    /**
     * 导出数据
     *
     * @param response
     * @throws IOException
     */
    @ApiOperation("导出所有用户")
    @GetMapping("/exportAllUser")
    public void exportExcel(@RequestParam(value = "role",required = false)  String roleName,
                                        HttpServletResponse response) throws IOException {
        List<User> users;
        Page<Object> page = new Page<>(0, Integer.MAX_VALUE);
        if(roleName != null) {
            users = userService.getByRoleName(page, roleName).getRecords();
        }else{
            users = userService.list();
        }
        ExcelUtils.exportExcel(users, "用户名单", "用户名单", User.class,"用户名单", response);
    }

    /**
     * 导出数据
     *
     * @throws IOException
     */
    @ApiOperation("导入用户")
    @PostMapping("/importUser")
    public CommonResult  importUser(@RequestParam("file") MultipartFile file) {
        try {
            List<UserDTO> users = ExcelUtils.importExcel(file, UserDTO.class);
            users.forEach(user-> {
                if(StringUtils.isBlank(user.getUsername())){
                    throw new CustomException("用户名不能为空");
                }
                if(StringUtils.isBlank(user.getEmail())){
                    throw new CustomException("邮箱不能为空");
                }
                user.setId(null);
                user.setPassword("123456");
                ArrayList<Integer> list = new ArrayList<>();
                list.add(3);
                user.setRoleIds(list);
            });
            userService.addUserList(users);
//        } catch (SQLIntegrityConstraintViolationException e){
//            return CommonResult.badArgument("邮箱重复");
        } catch (IOException e) {
            return CommonResult.badArgument("excel格式错误");
        }


        return CommonResult.success();
//        userService.saveBatch(users);
    }


    @ApiOperation("查看自己的个人信息")
    @GetMapping("/myProfile")
    public CommonResult myProfile(@ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        User thisUser = userService.getById(user.getId());
        if(thisUser != null) {
            thisUser.setPassword(null);
//            thisUser.setAge((int)LocalDateTime.now().until(thisUser.getBirthday(), ChronoUnit.YEARS));
        }
        return CommonResult.success(thisUser);
    }

    @ApiOperation("管理员更新某个用户个人信息")
    @PostMapping("updateUserProfile/{id}")
    public CommonResult<Integer> updateUserProfile(@PathVariable Integer id,  @RequestBody User user){
        user.setId(id);
        if(user.getPassword() != null ){
            int length = user.getPassword().trim().length();
            if(length > 20 || length < 6)
                return CommonResult.badArgument("密码应该在6到20个字符之间");
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword().trim()));
        }
        userService.updateById(user);
        return CommonResult.success();
    }

    @ApiOperation("更新用户个人信息")
    @PostMapping("updateMyProfile")
    public CommonResult updateMyProfile(@ApiIgnore Authentication authentication,  @RequestBody User user){
        User user1 = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("用户未登录");
        user.setId(user1.getId());
        if(user.getPassword() != null ){
            int length = user.getPassword().trim().length();
            if(length > 20 || length < 6)
                return CommonResult.badArgument("密码应该在6到20个字符之间");
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword().trim()));
        }
        userService.updateById(user);
        return CommonResult.success();
    }

    @ApiOperation("分页查询用户")
    @GetMapping("page")
    public CommonResult<Page<User>> findByPage(
                                    @RequestParam(required = false) String nickname,
                                    @RequestParam(required = false) String phone,
                                    @RequestParam(required = false) String username,
                                    @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                  @RequestParam(required = false, defaultValue = "10") Integer pageSize){

        Page<User> page = new Page<>();
        page.setSize(pageSize);
        page.setCurrent(pageNum);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        if(!StringUtils.isBlank(nickname))
        wrapper.like("nickname", nickname + "%");
        if(!StringUtils.isBlank(phone))
        wrapper.like("phone", phone + "%");
        if(!StringUtils.isBlank(username))
        wrapper.like("username",username + "%");
        Page<User> result = userService.page(page, wrapper);
//        result.getRecords().forEach(user ->
//                user.setAge((int)LocalDateTime.now().until(user.getBirthday(), ChronoUnit.YEARS)));
        return CommonResult.success(result);
    }

    @ApiOperation("分页查询老师")
    @GetMapping("/getTeacherPage")
    public CommonResult<IPage<User>> getTeachers(
            @RequestParam(required = false) String teacherName,
            @RequestParam(required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<User> page = new Page<User>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        if(StringUtils.isBlank(teacherName)) teacherName = null;
        User user = User.builder().nickname(teacherName).build();

        return CommonResult.success(userService.getTeachers(page, user));
    }

    @ApiOperation("分页查询学生")
    @GetMapping("/getStudentPage")
    public CommonResult<IPage<User>> getStudentPage(
            @RequestParam(required = false) String nickname,
            @RequestParam(required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<User> page = new Page<User>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        User user = User.builder().nickname(nickname).build();

        return CommonResult.success(userService.getStudents(page, user));
    }

    @ApiOperation("分页查询学生")
    @GetMapping("/getAdminPage")
    public CommonResult<IPage<User>> getAdminPage(
            @RequestParam(required = false) String nickname,
            @RequestParam(required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Page<User> page = new Page<User>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        User user = User.builder().nickname(nickname).build();
        return CommonResult.success(userService.getAdmins(page, user));
    }

    @ApiOperation("新增用户")
    @PostMapping("addUser")
    public CommonResult<Integer> addUser(@Validated @RequestBody UserDTO user, BindingResult bindingResult){
        if(StringUtils.isBlank(user.getPassword())){
            return CommonResult.badArgument("密码不能为空");
        }
        List<Integer> roleIds = user.getRoleIds() == null ? new ArrayList<>() : user.getRoleIds();
        roleIds.add(3);
        return CommonResult.success(userService.addUser(user));
    }

    @ApiOperation("获取登录用户的角色")
    @GetMapping("/myRole")
    public CommonResult myRole(@ApiIgnore Authentication authentication){
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("请先进行登录");
        List<Role> roles = userService.getRoles(user);
        int roleId = roles.stream().mapToInt(Role::getId).min().getAsInt();
        return CommonResult.success(roleId);
    }



    @ApiOperation("删除用户")
    @DeleteMapping("{id}")
    public CommonResult<Integer> delete(@PathVariable Integer id){
        userService.removeById(id);
        return CommonResult.success();
    }

    @ApiOperation("修改密码")
    @PostMapping("changeMyPassword")
    public CommonResult changePassword(@Validated @RequestBody ResetPassword resetPassword,
                                                @ApiIgnore Authentication authentication,
                                               BindingResult bindingResult){
//        userService.verifyEmailCode(resetPassword.getEmail(), resetPassword.getCode());
        User user = UserUtils.getLoginUser(authentication);
        if(user == null) return CommonResult.unauthorized("请先进行登录");
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.set("password", bCryptPasswordEncoder.encode(resetPassword.getNewPassword()))
                .eq("id", user.getId());
        userService.update(wrapper);
        return CommonResult.success();
    }

    @ApiOperation("忘记密码")
    @PostMapping("resetPassword")
    public CommonResult<Integer> resetPassword(@Validated @RequestBody ResetPassword resetPassword,
                                               BindingResult bindingResult){
        userService.verifyEmailCode(resetPassword.getEmail(), resetPassword.getCode());
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.set("password", bCryptPasswordEncoder.encode(resetPassword.getNewPassword()))
        .eq("email", resetPassword.getEmail());
        userService.update(wrapper);
        return CommonResult.success();
    }

    @ApiOperation("重置密码")
    @PostMapping("initPassword/{userId}")
    public CommonResult<Integer> initPassword(@PathVariable Integer userId){
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.set("password", bCryptPasswordEncoder.encode("123456"))
                .eq("id",userId);
        userService.update(wrapper);
        return CommonResult.success();
    }


    @ApiOperation("学生注册")
    @PostMapping("register")
    public CommonResult<Integer> register(@Validated @RequestBody UserDTO user, BindingResult bindingResult){
        user.setRoleIds(List.of(3));
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("email", user.getEmail());
        QueryWrapper<User> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("username", user.getUsername());
        if(StringUtils.isBlank(user.getPassword()))
            return CommonResult.badArgument("密码不能为空");
        else if(user.getCode() == null)
            return CommonResult.badArgument("验证码不能为空");
        else if(StringUtils.isBlank(user.getEmail()))
            return CommonResult.badArgument("邮箱不能为空");
        else if(!userService.list(wrapper).isEmpty())
            return CommonResult.badArgument("邮箱已存在");
        else if(!userService.list(wrapper1).isEmpty())
            return CommonResult.badArgument("用户名已存在");
        userService.verifyEmailCode(user.getEmail(), user.getCode());
        return CommonResult.success(userService.addUser(user));
    }


    @ApiOperation("发送邮箱验证码")
    @PostMapping("/sendEmailCode")
    public CommonResult sendEmailCode(@RequestBody User user) {
        if(StringUtils.isBlank(user.getEmail())){
            return CommonResult.badArgument("邮箱不能为空");
        }
        userService.sendVerifyEmail(user.getEmail());
        return CommonResult.success();
    }
//    @ApiOperation("ces")
//    @GetMapping("test")
//    public CommonResult<Integer> test(){
//        iCourseTimetableService.list();
//        courseJoinService.list();
//
//        return CommonResult.success();
//    }
}
