package org.fzs.config;


import org.fzs.common.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.fzs.common.utils.CommonResult;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = CustomException.class)
    public CommonResult handleCustomException(CustomException exception) {
        return CommonResult.badArgument(exception.getMsg());
    }

    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public CommonResult handleMaxUploadSizeExceededException(MaxUploadSizeExceededException exception) {
        return CommonResult.badArgument("上传的文件不能大于100m");
    }
}
