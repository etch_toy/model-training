package org.fzs.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * @author Administrator
 * @version 1.0
 **/
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(securedEnabled = true,prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAccessTokenConverter jwtAccessTokenConverter;

    //认证管理器
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    //密码编码器
    @Bean
    public PasswordEncoder passwordEncoder() {
//        String idForEncode = "bcrypt";
//        Map<String, PasswordEncoder> encoders = new HashMap<>();
//        encoders.put(idForEncode, new BCryptPasswordEncoder());
//        encoders.put("noop", NoOpPasswordEncoder.getInstance());
//        encoders.put("pbkdf2", new Pbkdf2PasswordEncoder());
//        encoders.put("scrypt", new SCryptPasswordEncoder());
//        encoders.put("sha256", new StandardPasswordEncoder());
//        return new DelegatingPasswordEncoder(idForEncode, encoders);
        return new BCryptPasswordEncoder();
//        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    //安全拦截机制
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
//                .requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
//                .and()
//                .authorizeRequests()
//                .antMatchers("/getPublicKey").permitAll()
//                .antMatchers("/verify/**").permitAll()
                .anyRequest().permitAll()
//                .and()
//                .authorizeRequests().antMatchers("/login*").permitAll()
//                .anyRequest().authenticated()
//                .and()
                // SpringSecurity 将不会创建Session，但是如果应用中其他地方创建了Session，那么Spring Security将会使用它。
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
//                .oauth2Login(oauth2 ->
//                        oauth2.successHandler(authenticationSuccessHandler)
//                        .failureHandler(authenticationFailureHandler)
//                        .permitAll())
//
                .and()
                .formLogin(form -> form
                        .loginPage("/index.html")
                        .loginProcessingUrl("/login")
                        .permitAll()
                )
        ;

    }

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode("hzu506");
        System.out.println(encode);
//        String password = "aaa";
//        String encode = encoder.encode(password);
//        boolean a = encoder.matches("123456", "$2a$10$.isLbHgI0rfa0acUugSiRut1NK9bNKQ.b6QzVbxqR30n0vyqlrd9C");
//        System.out.println("a = " + a);
    }

}
