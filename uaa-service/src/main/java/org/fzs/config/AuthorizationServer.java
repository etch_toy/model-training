package org.fzs.config;

import lombok.RequiredArgsConstructor;
import org.fzs.service.SpringDataUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;
import java.util.*;

/**
 * @author Administrator
 * @version 1.0
 * 授权服务配置
 **/
@Configuration
@RequiredArgsConstructor
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

    final private TokenStore tokenStore;
//    @Autowired
//    private ClientDetailsService clientDetailsService;
//    @Autowired
//    private AuthorizationCodeServices authorizationCodeServices;

    final private AuthenticationManager authenticationManager;
    final private PasswordEncoder passwordEncoder;
    final private DataSource dataSource;
    final private SpringDataUserDetailsService userDetailsService;
    final private JwtAccessTokenConverter jwtAccessTokenConverter;

//    @Bean
//    @Primary
//    public ClientDetailsService clientDetailsService(DataSource dataSource) {
//        JdbcClientDetailsService clientDetailsService = new JdbcClientDetailsService(dataSource);
//        clientDetailsService.setPasswordEncoder(passwordEncoder);
//        return clientDetailsService;
//    }


    //客户端详情服务
    @Override
    public void configure(ClientDetailsServiceConfigurer clients)
            throws Exception {
//        clients.withClientDetails(clientDetailsService);
        clients.jdbc(dataSource)
                .passwordEncoder(passwordEncoder)
        ;
    }


    //令牌管理服务
    @Bean
    public AuthorizationServerTokenServices tokenService() {
        DefaultTokenServices service=new DefaultTokenServices();
        service.setClientDetailsService(new JdbcClientDetailsService(dataSource));//客户端详情服务
        service.setSupportRefreshToken(true);//支持刷新令牌
        service.setTokenStore(tokenStore);//令牌存储策略
        //令牌增强
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        List<TokenEnhancer> tokenEnhancers = new ArrayList<>();
//        tokenEnhancers.add(tokenEnhancer());
        tokenEnhancers.add(jwtAccessTokenConverter);
        tokenEnhancerChain.setTokenEnhancers(tokenEnhancers);
//        tokenEnhancerChain.setTokenEnhancers(Collections.singletonList(accessTokenConverter));
        service.setTokenEnhancer(tokenEnhancerChain);

        service.setAccessTokenValiditySeconds(7200); // 令牌默认有效期2小时
        service.setRefreshTokenValiditySeconds(259200); // 刷新令牌默认有效期3天
        return service;
    }


//    @Bean
//    public AuthorizationCodeServices authorizationCodeServices(DataSource dataSource) {
//        return new JdbcAuthorizationCodeServices(dataSource);//设置授权码模式的授权码如何存取
//    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .accessTokenConverter(jwtAccessTokenConverter)

                .tokenServices(tokenService())//令牌管理服务
                .authenticationManager(authenticationManager)//认证管理器
                .authorizationCodeServices(new JdbcAuthorizationCodeServices(dataSource))//授权码服务
                .userDetailsService(userDetailsService) // 用户信息服务
                .allowedTokenEndpointRequestMethods(HttpMethod.POST);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security){
        security
                .tokenKeyAccess("permitAll()")                    //oauth/token_key是公开
                .checkTokenAccess("permitAll()")                  //oauth/check_token公开
                .allowFormAuthenticationForClients()				//表单认证（申请令牌）
        ;
    }

    /**
     * 使用非对称加密算法对token签名
     */
//    @Bean
//    public JwtAccessTokenConverter jwtAccessTokenConverter() {
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        converter.setKeyPair(keyPair());
//
//        return converter;
//    }

    /**
     * 从classpath下的密钥库中获取密钥对(公钥+私钥)
     */
//    @Bean
//    public KeyPair keyPair() {
//        KeyStoreKeyFactory factory = new KeyStoreKeyFactory(
//                new ClassPathResource("youlai.jks"), "123456".toCharArray());
//        KeyPair keyPair = factory.getKeyPair(
//                "youlai", "123456".toCharArray());
//        return keyPair;
//    }

    /**
     * JWT内容增强
     */
//    @Bean
//    public TokenEnhancer tokenEnhancer() {
//        return (accessToken, authentication) -> {
//            Map<String, Object> map = new HashMap<>(2);
//            User user = (User) authentication.getUserAuthentication().getPrincipal();
//            map.put("userId", user.getId());
//            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(map);
//            return accessToken;
//        };
//    }



}
